//Page Text Formatting
SMM.TextFormat = function (size, italic, bold, underline, color, value, fontFamily, allign,fillcolor){
    this.m_iNode_TextSize = size?size:14;
    this.m_iIsItalic = italic?italic:0;
    this.m_iIsBold = bold?bold:0;
    this.m_iIsUnderline = underline?underline:0;
    this.m_strNode_TextColor = color?color:'0x000000';
    this.m_strNode_text = value?value:'';
    this.m_strNode_TextFontFamily = fontFamily?fontFamily:'Open Sans';
    this.m_strNode_TextAlign = allign?allign:'center';
    this.m_strNode_TextFillColor = fillcolor?fillcolor:'0xffffff';

}





