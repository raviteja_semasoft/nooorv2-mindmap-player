var curEbag, factor;
var imageObj, loadedcount, totalImages, isPageLoad = false;
var priorImgs = [],
    notesImage = {},
    complImgs = [],
    correctImgs = [],
    clipArtImgs = [],
    smileImgs = [],
    pImages = [],
    clipartSwf = [],
    GenImgs = [],
    windowWidth, windowHeight, copyPage, cutPage, propPage;
var isAnimating = false,
    appAssets;
var isInternetExplorer = false;
var mapRendered = false;


$(document).ready(function () {

    SMM.showLoader(true, 'Loading....');

    window.addEventListener('resize', resize);
    resize();


    $("#container").panzoom({
        $zoomIn: $(".zoom-in"),
        $zoomOut: $(".zoom-out"),
        $zoomRange: $(".zoom-range"),
        $reset: $(".reset")
    });

    var IEversion = detectIE();

    if (IEversion !== false) {
        //  alert(IEversion);
        isInternetExplorer = true;


    } else {

        isInternetExplorer = false;

    }

    loadMindmapAssets();

});



/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // IE 12 / Spartan
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge (IE 12+)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

var resize = function () {
    windowWidth = $(window).width(); //retrieve current window width
    windowHeight = $(window).height();
    $('#mainDiv').css({
        width: windowWidth + 'px',
        height: windowHeight + 'px'
    });
    var m = (windowWidth - $('#container').outerWidth()) / 2;
    var h = (windowHeight - $('#container').outerHeight()) / 2;
    $('#container').css("top", h + 'px');
    $('#container').css("left", m + 'px');
    var ebag = SMM.EBag();
    if (ebag) {

        try {
            SMM.EBag().hideAllMenus();
            var scale = SMM.EBag().m_scale;
            var t = (scale / 5 * 100) + '%';
            //  console.log(t);
            $('.zoom-range').css({
                "background": "#000",
                "backgroundImage": "linear-gradient(to right,  #fff " + t + ",#000 " + t + ")",
                background: "-moz-linear-gradient(left, #fff " + t + ",#000 " + t + ")",
                background: "-webkit-linear-gradient(left,  #fff " + t + ",#000 " + t + ")"
            });
        } catch (e) {
            console.log(e.message);
        }
    }
}





function mapFailed() {
    alert('Unable to load Map');
}

//Loading images from JS
function loadMindmapAssets() {
    var allAssets = mindMapImagesfun();
    priorImgs = allAssets.priorityImages;
    notesImage = allAssets.notesImages;
    complImgs = allAssets.completionImages;
    correctImgs = allAssets.correctImages;
    clipArtImgs = allAssets.clipartsImages;
    smileImgs = allAssets.smileImages;
    GenImgs = allAssets.generalImages;
    pImages = allAssets.iconImages;

    loadLessonXML('./Resources/MindMap.json?' + new Date().getTime());
}


function loadLessonXML(pageUrl) {
    $.ajax({
        url: pageUrl,
        type: "GET",

        success: function (data) {
            var curXML = data.SemaFileSystem;
            pageLoad(curXML);

        }
    });
}


function sortChildren(items) {
    items.sort(function (a, b) {
        return a.m_iNode_Order - b.m_iNode_Order;
    });
    return items;
}

var structureContent = [];

function modifyStructureJson(data) {
    var childrenContent = [];
    for (var i = 0; i < data.children.length; i++) {

        var childSemaNode = data.children[i].SemaNode;
        // console.log(childSemaNode);

        if (childSemaNode.children && childSemaNode.children.length > 0) {

            modifyStructureJson(childSemaNode);
        }

        childrenContent.push(childSemaNode);


        // var x = data.children[i].children
        // if (x && x.length) {
        //     modifyStructureJson(x.children);
        // }
    }

    data.children = sortChildren(childrenContent);
    console.log(data);
    return data;
}


var rootNodeLoaded = false;

var pageLoad = function (data) {

    // console.log(data);
    var eBagXML = data;

    rootNodeLoaded = false;
    SMM.stageMove();
    SMM.showLoader(false);
    var xmlStr;

    $('#container').empty();
    $('#container').panzoom("zoom", 1, {
        silent: true
    });

    resize();
    $('.reset').trigger('click');


    var pageXML = eBagXML.readAllSemaNodesinAMapResult;

    curEbag = new SMM.EBag(
        parseInt(eBagXML.m_iId),
        parseInt(eBagXML.m_iMapHeight),
        parseInt(eBagXML.m_iMapLayout),
        parseInt(eBagXML.m_iMapStyle),
        parseInt(eBagXML.m_iMapWidth),
        parseInt(eBagXML.m_iMaporFolder),
        parseInt(eBagXML.m_iNumberOfRightNodes),
        parseInt(eBagXML.m_iParentFolderId),
        eBagXML.m_isPublished,
        eBagXML.m_strAuthor,
        eBagXML.m_strComment,
        eBagXML.m_strCreatedDate,
        eBagXML.m_strMapDescription,
        eBagXML.m_strMapLayoutFillColor,
        eBagXML.m_strMapProperties,
        eBagXML.m_strModifiedDate,
        eBagXML.m_TabletPublishStatus,
        eBagXML.m_strName,
        true
    );



    var modifyContent = modifyStructureJson(pageXML[0].SemaNode);

    var objPageXML = modifyContent;
    // var objPageXML = pageXML.SemaNode;

    if (objPageXML == undefined) {
        console.log(pageXML);
        $('#errorHandler').css('display', 'block');
        // $('.errorHandleContent').val(JSON.stringify(pageXML));
        alert('Unable to render the map');
        createSampleMap();
        return;

    }

    var pageObj;
    var factor, originalXPos, originalYPos;

    var parentPage = pageXml(objPageXML, parentPage);


    function pageXml(objPageXML, parentPage) {
        // console.log("checking pages...." + objPageXML);

        var xPos = parseInt(objPageXML.m_fX);
        var yPos = parseInt(objPageXML.m_fY);

        var factorX = 1,
            factorY = 1;

        // if(curEbag.m_iMapLayout == 3 && rootNodeLoaded)// if Freemap layout we needs factor so that map will render same across all screen sizes
        if (rootNodeLoaded) {
            var rootNodeXPos = curEbag.m_pages[0].m_x,
                rootNodeYPos = curEbag.m_pages[0].m_y;
            var xFactor = originalXPos - rootNodeXPos;
            var yFactor = originalYPos - rootNodeYPos;
            xPos -= xFactor;
            yPos -= yFactor; //xPos/factorX, yPos = yPos/factorY;

        }



        pageObj = new SMM.Page(curEbag, //objPageXML.LEVELID|objPageXML.levelID,
            objPageXML.children,
            parseInt(objPageXML.m_iId),
            parseInt(objPageXML.m_iMapId),
            parseInt(objPageXML.m_iParentId),
            xPos,
            yPos,
            parseInt(objPageXML.m_iNode_Order),
            objPageXML.m_boolPinned,
            objPageXML.m_iPriority,
            objPageXML.m_iCompletion,
            objPageXML.m_strStartDate,
            objPageXML.m_strDueDate,
            objPageXML.m_strResourceType,
            objPageXML.m_iResourceDuration,
            objPageXML.m_strDurationType,
            objPageXML.m_strMultimediaType,
            objPageXML.m_strMultimediaUrl,
            objPageXML.m_strHyperlinkUrl,
            objPageXML.m_strHyperlinkDescription,
            objPageXML.m_strOpenLinkIn,
            objPageXML.m_iSymbol,
            objPageXML.m_strNode_Notes,
            objPageXML.m_iIsAttachment,
            objPageXML.m_iNodeShape,
            objPageXML.m_strNodeShapeFill,
            objPageXML.m_iSmileys,
            objPageXML.m_iFlags,
            objPageXML.m_iCheckedStatus,
            objPageXML.m_lImage,
            objPageXML.m_strShapeLineColor,
            objPageXML.m_iShapeLineThickness,
            objPageXML.m_fNodeShapeFillColorAlpha,
            objPageXML.m_strAttachments,
            objPageXML.m_iImageHeight,
            objPageXML.m_iImageWidth,
            objPageXML.m_iBoundary,
            objPageXML.m_strBoundaryColor,
            objPageXML.m_iType,
            objPageXML.m_iGradientType,
            parseInt(objPageXML.m_iIsAboveCenter));


        var textObj = new SMM.TextFormat(
            objPageXML.m_iNode_TextSize,
            objPageXML.m_iIsItalic,
            objPageXML.m_iIsBold,
            objPageXML.m_iIsUnderline,
            objPageXML.m_strNode_TextColor,
            objPageXML.m_strNode_text,
            objPageXML.m_strNode_TextFontFamily,
            objPageXML.m_strNode_TextAlign,
            objPageXML.m_strNode_TextFillColor


        );

        pageObj.m_textFormat = textObj;

        if (parentPage) {
            parentPage.addPage(pageObj); //Default .. Circular/Left/Right
        } else {

            originalXPos = parseInt(objPageXML.m_fX);
            originalYPos = parseInt(objPageXML.m_fY);
            pageObj.m_levelId = 0;
            curEbag.addPage(pageObj);
            rootNodeLoaded = true;
        }
        parentPage = pageObj;

        console.log(pageObj.m_textFormat.m_strNode_text + ': ' + pageObj.m_iIsAboveCenter)

        if (objPageXML.children && objPageXML.children.length > 0) {
            for (var i = 0; i < objPageXML.children.length; i++) {
                pageXml(objPageXML.children[i], parentPage);

            }

        }

        return parentPage;

    }

    isPageLoad = false;
    isAnimate = false;



    createBackGroundColor();
    SMM.animateLayout();

    setTimeout(function () {
        SMM.layout();
    }, 1000);
}


function createBackGroundColor() {
    if (curEbag.m_strMapLayoutFillColor && curEbag.m_strMapLayoutFillColor.length > 0) {
        var bgColor;
        if ((curEbag.m_strMapLayoutFillColor).indexOf('#') >= 0) {
            bgColor = curEbag.m_strMapLayoutFillColor;
        } else {
            bgColor = '#' + ('00000' + (curEbag.m_strMapLayoutFillColor | 0).toString(16)).substr(-6);
        }
        $('#container').css('background', bgColor);
        $('#mainDiv').css('backgroundColor', bgColor);
    }

}



/*******************************************************************************/
/*                      Page Load Handlers                            */
/********************************************************************************/



SMM.resetBounds = function () {

    minX = 0;
    maxX = 0;
    minY = 0;
    maxY = 0;
}


SMM.showLoader = function (value, text) {


    if (value) {
        $('#loaderDiv').css('display', 'block');
        $('#loader').css('display', 'block');
        //  $(".panel").addClass("opacity");

        if (text)
            $("#loaderTxt").text(text);
    } else {
        $('#loader').css('display', 'none');
        $('#loaderDiv').css('display', 'none');
        $("#loaderTxt").text('');
    }
}