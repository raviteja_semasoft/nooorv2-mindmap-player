var isReorderPosition = false,
    callcount = 0;

SMM.circularLayout = function () {

    var curEbag = SMM.EBag();
    SMM.resetBounds();
    var rootNode = curEbag.m_pages[0].m_group;
    var h = $(rootNode).outerHeight();
    var rootNodeWidth = $(rootNode).outerWidth();
    var centerX = ($('#container').innerWidth() / 2);
    var centerY = ($('#container').innerHeight() / 2);
    rootNode.style.left = centerX - rootNodeWidth / 2 + 'px';
    rootNode.style.top = centerY - h / 2 + 'px';

    SMM.getChildrenHeight(curEbag.m_pages[0], curEbag.m_pages[0].m_children);
    SMM.resetLineArray();
    SMM.updateNodePositions();
    curEbag.m_rightPages.length = 0;
    curEbag.m_leftPages.length = 0;


    //3
    if (isPageLoad == true)
        return;




    var yOff_rt = 0.0,
        yOff_rb = 0.0,
        yOff_lt = 0.0,
        yOff_lb = 0.0;
    var top = $(curEbag.m_pages[0].m_group).positionHack().top;

    SMM.setPositionsRightMap(curEbag.m_pages[0], arrRightTop, top - rtHeight - 50 - yOff_rt);
    SMM.setPositionsRightMap(curEbag.m_pages[0], arrRightBottom, top + 50 + curEbag.m_pages[0].m_selfHeight - yOff_rb);
    SMM.setPositionsLeftMap(curEbag.m_pages[0], arrLeftTop, top - ltHeight - 50 - yOff_lt);
    SMM.setPositionsLeftMap(curEbag.m_pages[0], arrLeftBottom, top + 50 + curEbag.m_pages[0].m_selfHeight - yOff_lb);



    if (curEbag.m_iMapStyle == 2)
        SMM.drawLine();
    else if (curEbag.m_iMapStyle == 3)
        SMM.drawThickerCurve();
    else if (curEbag.m_iMapStyle == 1)
        SMM.drawStraightLine();



    if (curEbag.m_currPageBeingDragged != undefined) {
        //Find the nearest page on the side m_currPageBeingDragged is and draw a line to its parent based on relateive x position.
        var currPageBeingDraggerPos = $(curEbag.m_currPageBeingDragged.m_group).positionHack();
        var currPageBeingDraggerWidth = $(curEbag.m_currPageBeingDragged.m_group).outerWidth();
        var rootNodePos = $(rootNode).positionHack();
        if (currPageBeingDraggerPos.left + currPageBeingDraggerWidth / 2 > //for checking whether dragged page is on right or left
            rootNodePos.left + rootNodeWidth / 2) {

            curEbag.m_nearestPageToTheCurrentPageBeingDragged = curEbag.m_currPageBeingDragged.m_parentPage;

            if (nearestShape != undefined) {
                nearestShape.m_group.classList.remove('activeNode');
                nearestShape = undefined;
            }
            nearestShape = SMM.hitTestShape(curEbag.m_currPageBeingDragged);

            if (nearestShape != undefined) {

                if (curEbag.m_currPageBeingDragged.m_parentPage != nearestShape) {
                    if ((nearestShape.m_parentPage == undefined) ||
                        (nearestShape.m_parentPage.m_levelId != 0)) {

                        nearestShape.m_group.classList.add('activeNode');
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    } else {
                        nearestShape.m_group.classList.add('activeNode');
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    }
                }



            }
            //Draw the line here
            var thisEBag = curEbag;
            //Needs to validate below code as Single line drawing functionality requires tweeking.
            //var lpoint = new SMM.SLine();
            //    var parentPage =  thisEBag.m_currPageBeingDragged;
            //    var xFrom = $(parentPage.m_group).positionHack().left;
            //    var yFrom = $(parentPage.m_group).positionHack().top;
            //    var nearestPage = thisEBag.m_nearestPageToTheCurrentPageBeingDragged;
            //    var nearestPageX = $(nearestPage.m_group).positionHack().left,
            //        nearestPageY = $(nearestPage.m_group).positionHack().top;
            //if (thisEBag.m_nearestPageToTheCurrentPageBeingDragged.m_parentPage == undefined) {
            //
            //    lpoint.moveTo(xFrom + parentPage.m_selfWidth / 2, yFrom);
            //    lpoint.qCurveto(xFrom + parentPage.m_selfWidth + 10 +60,
            //                    yFrom + parentPage.m_selfHeight/2,
            //                    nearestPageX + parentPage.m_selfWidth/4,
            //                    nearestPageY + parentPage.m_selfHeight/2);
            //} else {
            //    lpoint.moveTo(xFrom + parentPage.m_selfWidth / 2, yFrom);
            //    lpoint.qCurveto(xFrom + parentPage.m_selfWidth + 10 +25,
            //        yFrom + parentPage.m_selfHeight/2,
            //        nearestPageX + parentPage.m_selfWidth/4,
            //        nearestPageY + parentPage.m_selfHeight/2);
            //}
            //lpoint.clr = '#FF0000';
            //lpoint.type = 2;
            //arrGLPoints.push(lpoint);
            //SMM.drawLine();



        } else {

            curEbag.m_nearestPageToTheCurrentPageBeingDragged = curEbag.m_currPageBeingDragged.m_parentPage;
            //   console.log('>>>>>>>>>>>>>>>>else part>>>>>>>>>>>>>>>>');
            if (nearestShape != undefined) {
                nearestShape.m_group.classList.remove('activeNode');
                nearestShape = undefined;
            }
            nearestShape = SMM.hitTestShape(curEbag.m_currPageBeingDragged);

            if (nearestShape != undefined) {
                if (curEbag.m_currPageBeingDragged.m_parentPage != nearestShape) {

                    if ((nearestShape.m_parentPage == undefined) ||
                        (nearestShape.m_parentPage.m_levelId != 0)) {
                        nearestShape.m_group.classList.add('activeNode');
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    } else {
                        nearestShape.m_group.classList.add('activeNode');
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    }

                }



            }




        }
    }

    curEbag.caluculatePos(curEbag.m_pages[0]); //For x and Y pos

}



SMM.updateNodePositions = function () {

    var curEbag = SMM.EBag();

    arrRightTop.length = 0;
    rtHeight = 0;
    arrRightBottom.length = 0;
    rbHeight = 0;
    arrLeftBottom.length = 0;
    lbHeight = 0;
    arrLeftTop.length = 0;
    ltHeight = 0;

    var ct = 0;
    var rootNode = curEbag.m_pages[0];
    while (rootNode.m_children.length > ct && ct < curEbag.m_iNumberOfRightNodes && rootNode.m_children[ct] && rootNode.m_children[ct].m_iIsAboveCenter == 1) {
        arrRightTop.push(rootNode.m_children[ct]);
        rtHeight += rootNode.m_children[ct].m_childrenHeight;
        rootNode.m_children[ct].m_strLocation = '' + (ct);
        ct++;
    }

    while (rootNode.m_children.length > ct && ct < curEbag.m_iNumberOfRightNodes && rootNode.m_children[ct] && rootNode.m_children[ct].m_iIsAboveCenter == 0) {
        arrRightBottom.push(rootNode.m_children[ct]);
        rbHeight += rootNode.m_children[ct].m_childrenHeight;
        rootNode.m_children[ct].m_strLocation = '' + (ct);
        ct++;
    }

    ct = rootNode.m_children.length - 1;

    while (ct > curEbag.m_iNumberOfRightNodes - 1 && rootNode.m_children[ct] && rootNode.m_children[ct].m_iIsAboveCenter == 1) {
        arrLeftTop.push(rootNode.m_children[ct]);
        ltHeight += rootNode.m_children[ct].m_childrenHeight;
        rootNode.m_children[ct].m_strLocation = '' + (ct);
        ct--;
    }

    while (ct > curEbag.m_iNumberOfRightNodes - 1 && rootNode.m_children[ct] && rootNode.m_children[ct].m_iIsAboveCenter == 0) {
        arrLeftBottom.push(rootNode.m_children[ct]);
        lbHeight += rootNode.m_children[ct].m_childrenHeight;
        rootNode.m_children[ct].m_strLocation = '' + (ct);
        ct--;
    }



    console.log(arrRightTop);
    console.log(arrRightBottom);
    console.log(arrLeftTop);
    console.log(arrLeftBottom);

}




SMM.getRealChildrenHeight = function (children) {
    var childrenht = 0;
    for (var i = 0; i < children.length; i++) {
        childrenht += children[i].m_childrenHeight;
    }
    return childrenht;
}


SMM.setPositionsRightMap = function (parentPage, pages, startY, collapse) {
    var rootNodePos = $(parentPage.m_group).positionHack();
    var curEbag = SMM.EBag();
    var parentWidth = $(parentPage.m_group).outerWidth();
    parentPage.m_selfWidth = parentWidth;

    var xPos = rootNodePos.left + parentWidth + 40,
        yPos;


    if (startY == -9999999) {
        var childHgt = SMM.getRealChildrenHeight(parentPage.m_children);

        if (childHgt > parentPage.m_childrenHeight) {
            yPos = rootNodePos.top - parentPage.m_childrenHeight / 2 + parentPage.m_selfHeight / 2;
        } else {
            yPos = rootNodePos.top - childHgt / 2 + parentPage.m_selfHeight / 2;

        }
    } else {
        yPos = startY;
    }

    var arrPoints = [];
    var draggedLayerPosition = -1;
    for (var i = 0; i < pages.length; i++) {
        yPos = yPos + pages[i].m_childrenHeight / 2;

        if (parentPage.m_levelId != 0)
            pages[i].m_strLocation = parentPage.m_strLocation + '/' + i;

        if (pages[i] != curEbag.m_currPageBeingDragged) {
            if (parentPage.m_isCollapsed == true || collapse == true) {
                pages[i].m_group.style.display = 'none';
                SMM.setPositionsRightMap(pages[i], pages[i].m_children, -9999999, true);

            } else {
                pages[i].m_group.style.display = 'flex';
                pages[i].m_group.style.display = '-webkit-flex';
                pages[i].setPosition(xPos, yPos - pages[i].m_selfHeight / 2.0);
                var obj = {
                    x: xPos,
                    y: yPos - 2.5,
                    page: pages[i]
                };
                //arrPoints.push(xPos);
                //arrPoints.push(yPos - 2.5);
                obj.page.m_selfWidth = obj.page.m_selfWidth / 2;
                console.log('!!!!!!!!!!!!!!!');
                arrPoints.push(obj);
                SMM.setPositionsRightMap(pages[i], pages[i].m_children, -9999999, false);

            }

            curEbag.m_rightPages.push(pages[i]);

        } else {
            draggedLayerPosition = i * 2;

            var currentDraggedNode = $(curEbag.m_currPageBeingDragged.m_group).positionHack();
            if (currentDraggedNode.left + curEbag.m_currPageBeingDragged.m_selfWidth / 2 > rootNodePos.left + parentWidth) {
                curEbag.m_rightPages.push(pages[i]);
                SMM.setPositionsRightMap(pages[i], pages[i].m_children, -9999999);
                var pagePos = $(pages[i].m_group).positionHack();

                var obj = {
                    x: pagePos.left,
                    y: pagePos.top + pages[i].m_selfHeight / 2,
                    page: pages[i]
                };

                arrPoints.push(obj);
                //arrPoints.push(pagePos.left);
                //arrPoints.push(pagePos.top + pages[i].m_selfHeight / 2);

            } else {
                curEbag.m_leftPages.push(pages[i]);
                SMM.setPositionsLeftMap(pages[i], pages[i].m_children, -9999999);
                var pagePos = $(pages[i].m_group).positionHack();
                //arrPoints.push(pagePos.left + pages[i].m_selfWidth + 10);
                //arrPoints.push(pagePos.top + pages[i].m_selfHeight / 2);
                var obj = {
                    x: pagePos.left + pages[i].m_selfWidth + 10,
                    y: pagePos.top + pages[i].m_selfHeight / 2,
                    page: pages[i]
                };
                arrPoints.push(obj);


            }

        }
        yPos = yPos + pages[i].m_childrenHeight / 2; // + parentPage.m_selfHeight/2;;
    }

    var rootNodePos2 = $(parentPage.m_group).positionHack();
    var xFrom = rootNodePos2.left + parentPage.m_selfWidth;
    var yFrom = rootNodePos2.top + parentPage.m_selfHeight / 2 - 2.5;



    for (var j = 0; j < arrPoints.length; j++) {

        if (draggedLayerPosition != j) {

            var lpoint = new SMM.SLine();

            var sign = ((xFrom > arrPoints[j].x) ? 1 : -1);

            var xOffset = 0;
            var node = arrPoints[j].page;
            if (node && node.m_children.length > 0) {
                xOffset = node.m_selfWidth;
            } else {
                xOffset = node.m_selfWidth / 2;
            }

            if (parentPage.m_levelId == 0) {
                lpoint.moveTo(xFrom - parentPage.m_selfWidth / 2, yFrom);
                lpoint.qCurveto(arrPoints[j].x + sign * 25 - parentPage.m_selfWidth / 2, arrPoints[j].y, arrPoints[j].x, arrPoints[j].y);

            } else {
                lpoint.moveTo(xFrom, yFrom);
                lpoint.qCurveto(arrPoints[j].x + sign * 25, arrPoints[j].y, arrPoints[j].x, arrPoints[j].y);
            }
            lpoint.clr = parentPage.m_strNodeShapeFill;
            lpoint.type = 1;
            lpoint.lineTo(arrPoints[j].x + xOffset, arrPoints[j].y);
            arrGLPoints.push(lpoint);

        }


    }
}


SMM.setPositionsLeftMap = function (parentPage, pages, startY, collapse) {
    var curEbag = SMM.EBag();
    var rootNodePos = $(parentPage.m_group).positionHack();
    var parentWidth = $(parentPage.m_group).outerWidth();
    parentPage.m_selfWidth = parentWidth;
    var xPos = rootNodePos.left - 40;
    var yPos;

    if (startY == -9999999) {
        var childHgt = SMM.getRealChildrenHeight(parentPage.m_children);

        if (childHgt > parentPage.m_childrenHeight) {
            yPos = rootNodePos.top - parentPage.m_childrenHeight / 2 + parentPage.m_selfHeight / 2;
        } else {
            yPos = rootNodePos.top - childHgt / 2 + parentPage.m_selfHeight / 2;

        }
    } else {
        yPos = startY;
    }

    var arrPoints = [];
    var draggedLayerPosition = -1;
    for (var i = 0; i < pages.length; i++) {
        yPos = yPos + pages[i].m_childrenHeight / 2;
        if (parentPage.m_levelId != 0)
            pages[i].m_strLocation = parentPage.m_strLocation + '/' + i;

        if (pages[i] != curEbag.m_currPageBeingDragged) {
            if (parentPage.m_isCollapsed == true || collapse == true) {
                pages[i].m_group.style.display = 'none';
                SMM.setPositionsLeftMap(pages[i], pages[i].m_children, -9999999, true);

            } else {
                pages[i].m_group.style.display = 'flex';
                pages[i].m_group.style.display = '-webkit-flex';
                pages[i].setPosition(xPos - pages[i].m_selfWidth, yPos - pages[i].m_selfHeight / 2.0);
                var obj = {
                    x: xPos,
                    y: yPos - 2.5,
                    page: pages[i]
                };
                //arrPoints.push(xPos);
                //arrPoints.push(yPos - 2.5);
                obj.page.m_selfWidth = obj.page.m_selfWidth / 2;
                arrPoints.push(obj);
                curEbag.m_leftPages.push(pages[i]);
                SMM.setPositionsLeftMap(pages[i], pages[i].m_children, -9999999, false);
            }
        } else {
            draggedLayerPosition = i * 2;
            var currentDraggedNode = $(curEbag.m_currPageBeingDragged.m_group).positionHack();

            if (currentDraggedNode.left + curEbag.m_currPageBeingDragged.m_selfWidth / 2 > rootNodePos.left + parentWidth) {
                curEbag.m_rightPages.push(pages[i]);
                SMM.setPositionsRightMap(pages[i], pages[i].m_children, -9999999); //Why Rightmap here?

                var pagePos = $(pages[i].m_group).positionHack();
                //arrPoints.push(pagePos.left);
                //arrPoints.push(pagePos.top + pages[i].m_selfHeight / 2);
                var obj = {
                    x: pagePos.left,
                    y: pagePos.top + pages[i].m_selfHeight / 2,
                    page: pages[i]
                };
                arrPoints.push(obj);


            } else {
                curEbag.m_leftPages.push(pages[i]);
                SMM.setPositionsLeftMap(pages[i], pages[i].m_children, -9999999);
                var pagePos = $(pages[i].m_group).positionHack();
                //arrPoints.push(pagePos.left + pages[i].m_selfWidth + 10);
                //arrPoints.push(pagePos.top + pages[i].m_selfHeight / 2);

                var obj = {
                    x: pagePos.left + pages[i].m_selfWidth + 10,
                    y: pagePos.top + pages[i].m_selfHeight / 2,
                    page: pages[i]
                };
                arrPoints.push(obj);



            }
        }
        yPos = yPos + pages[i].m_childrenHeight / 2;
    }

    var rootNodePos2 = $(parentPage.m_group).positionHack();
    var xFrom = rootNodePos2.left;
    var yFrom = rootNodePos2.top + parentPage.m_selfHeight / 2 - 2.5;



    for (var j = 0; j < arrPoints.length; j++) {
        if (draggedLayerPosition != j) {

            var sign = ((xFrom > arrPoints[j].x) ? 1 : -1);

            var lpoint = new SMM.SLine();
            var xOffset = 0;
            var yOffset = 2;
            var node = arrPoints[j].page;
            if (node && node.m_children.length > 0) {
                xOffset = node.m_selfWidth;
            } else {
                xOffset = node.m_selfWidth / 2;
            }

            if (parentPage.m_levelId == 0) {

                lpoint.moveTo(xFrom + parentPage.m_selfWidth / 2, yFrom);
                lpoint.qCurveto(arrPoints[j].x + sign * 25 + parentPage.m_selfWidth / 2, arrPoints[j].y, arrPoints[j].x, arrPoints[j].y);
            } else {
                lpoint.moveTo(xFrom, yFrom);
                lpoint.qCurveto(arrPoints[j].x + sign * 25, arrPoints[j].y, arrPoints[j].x, arrPoints[j].y);
            }
            lpoint.clr = parentPage.m_strNodeShapeFill;
            lpoint.type = 2;
            lpoint.lineTo(arrPoints[j].x - xOffset, arrPoints[j].y);
            arrGLPoints.push(lpoint);
        }
    }

}


SMM.getChildrenHeight = function (parentPage, childPages) {
    var childrenHeight = 0;
    for (var i = 0; i < childPages.length; i++) {
        SMM.getChildrenHeight(childPages[i], childPages[i].m_children);
        childrenHeight = childrenHeight + childPages[i].m_childrenHeight;
    }
    if (childrenHeight > parentPage.m_selfHeight)
        parentPage.m_childrenHeight = childrenHeight;
    else parentPage.m_childrenHeight = parentPage.m_selfHeight;
    if (childrenHeight == 0 || parentPage.m_isCollapsed == true)
        parentPage.m_childrenHeight = parentPage.m_selfHeight + 38;
}


SMM.reArrangeChildPos = function (parentPage) {

    var curEbag = SMM.EBag();

    function resetChildren(parentPage) {

        parentPage.m_children.forEach(function (page) {
            resetChildren(page);
        });

        parentPage.m_children.length = 0;
    }

    curEbag.layout();
}


//Drawing SVG curved Lines
SMM.drawLine = function () {

    var w = $('#container').outerWidth(),
        h = $('#container').outerHeight();
    //if($('#container svg').length == 0)
    //{
    //    $('#container').append($('<svg height='+h+' width='+w+' style="position: absolute;"> </svg>'));
    //}
    arrGLPoints.forEach(function (sline) {
        //Dynamically creating Path for SVG Line drawings
        var svgLine = 'M ' + sline.mx + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy + 'L' + sline.mx1 + ' ' + sline.my1;
        var path = '<path d="' + svgLine + '" stroke="' + sline.clr + '" stroke-width="2" fill="none"  stroke-width="2" stroke-linecap="round" opacity="1" />';
        $('#container').append($('<svg height=' + h + ' width=' + w + ' style="position: absolute;"> ' +
            path +
            '</svg>'));

    });

}


//Drawing SVG Straight Lines

SMM.drawStraightLine = function () {
    arrGLPoints.forEach(function (sline) {
        //Dynamically creating Path for SVG Line drawings
        var variableVal = 0;
        if (sline.type == 1)
            variableVal = -15;
        else
            variableVal = 15;
        var svgLine = 'M ' + sline.mx + ' ' + sline.my + ' ' + 'L ' + (sline.cpx + variableVal) + ' ' + sline.cpy + '  ' + 'L ' + sline.qcx + ' ' + sline.qcy + 'L' + sline.mx1 + ' ' + sline.my1;

        var w = $('#container').outerWidth(),
            h = $('#container').outerHeight();
        $('#container').append($('<svg height=' + h + ' width=' + w + ' style="position: absolute;"> ' +
            '<path d="' + svgLine + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +

            '</svg>'));

    });
}

// Drawing SVG Thicker Curve
SMM.drawThickerCurve = function () {

    arrGLPoints.forEach(function (sline) {
        //Dynamically creating Path for SVG Line drawings
        var svgLine1 = 'M ' + (sline.mx + 0) + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy;
        var svgLine2 = 'M ' + (sline.mx + 1) + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy;
        var svgLine3 = 'M ' + (sline.mx + 2) + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy + 'L' + sline.mx1 + ' ' + sline.my1;
        var svgLine4 = 'M ' + (sline.mx + 3) + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy;
        var svgLine5 = 'M ' + (sline.mx + 4) + ' ' + sline.my + ' ' + 'Q ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.qcx + ' ' + sline.qcy;
        var w = $('#container').outerWidth(),
            h = $('#container').outerHeight();
        $('#container').append($('<svg height=' + h + ' width=' + w + ' style="position: absolute;"> ' +
            '<path d="' + svgLine1 + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '<path d="' + svgLine2 + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '<path d="' + svgLine3 + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '<path d="' + svgLine4 + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '<path d="' + svgLine5 + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '</svg>'));

    });
}