var nearestShape, draggedShape, isAnimate, isreCaluculate, callcount1 = 0;
SMM.freeMapLayout = function () {

    var curEbag = SMM.EBag();
    SMM.resetLineArray();


    var rootNode = curEbag.m_pages[0].m_group;
    var w = $(rootNode).outerWidth();
    var h = $(rootNode).outerHeight();


    //curEbag.m_pages[0].m_x = $('#container').outerWidth() / 2 - w / 2;
    //curEbag.m_pages[0].m_y = $('#container').outerHeight() / 2 - h / 2
    //
    //// curEbag.m_pages[0].m_group.setPosition(curEbag.m_stageLayer.getWidth()/2-curEbag.m_pages[0].m_selfWidth/2,curEbag.m_stageLayer.getHeight()/2-curEbag.m_pages[0].m_selfHeight/2);
    //
    //rootNode.style.left = $('#container').outerWidth() / 2 - w / 2 + 'px';
    //rootNode.style.top = $('#container').outerHeight() / 2 - h / 2 + 'px';

    // if(curEbag.m_currPageBeingDragged == undefined)
    {

        SMM.setPositions(curEbag.m_pages[0], curEbag.m_pages[0].m_children, curEbag.m_isCollapsed);
    }


    SMM.drawDashedLine();
    if (curEbag.m_currPageBeingDragged != undefined) {


        draggedShape = curEbag.m_currPageBeingDragged;

        if (curEbag.m_currPageBeingDragged.m_parentPage == undefined) return;

        if (nearestShape != undefined) {
            nearestShape.resetStyle();
            nearestShape = undefined;
        }

        nearestShape = SMM.hitTestShape(curEbag.m_currPageBeingDragged);

        if (nearestShape != undefined) {

            if (curEbag.m_currPageBeingDragged.m_parentPage != nearestShape) {
                if ((nearestShape.m_parentPage == undefined) ||
                    (nearestShape.m_parentPage.m_levelId != 0)) {

                    nearestShape.highlight();
                    curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                } else {
                    nearestShape.highlight();
                    curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                }
            }



        }

        curEbag.m_nearestPageToTheCurrentPageBeingDragged = curEbag.m_currPageBeingDragged.m_parentPage;


    }
    //  curEbag.layout();
}


SMM.hitTestShape = function (target) {


    var curEbag = SMM.EBag();
    var shapes = document.getElementsByClassName("node");
    var isIntersect = false;
    var intersectShape = undefined;
    var shape;

    var rect1;

    if (target.m_group != undefined) {
        rect1 = target.m_group.getBoundingClientRect();
    } else {
        rect1 = target;
    }


    var rect2 = undefined;
    for (var i = 0; i < shapes.length; i++) {
        shape = shapes[i];
        rect2 = undefined;

        if (shape != target.m_group) {
            var rect2 = {};
            if (target.m_group == undefined) {
                var shapePos = $(shape).positionHack();
                var shapeWidth = $(shape).outerWidth();
                var shapeHeight = $(shape).outerHeight();


                rect2 = {
                    left: shapePos.left,
                    top: shapePos.top,
                    right: shapePos.left + shapeWidth + 6,
                    bottom: shapePos.top + shapeHeight + 6
                }
            } else {


                var rectValus = shape.getBoundingClientRect();
                rectValus.right = rectValus.right + 10;
                rectValus.bottom = rectValus.bottom + 10;
                rect2 = rectValus;


            }

            isIntersect = intersectRect(rect1, rect2);
        }

        rect2 = undefined;


        if (isIntersect) {
            intersectShape = $(shape).data().page;

            break;
        }


    }

    rect1 = undefined;
    return intersectShape;

}


function intersectRect(a, b) {
    return (a.left <= b.right &&
        b.left <= a.right &&
        a.top <= b.bottom &&
        b.top <= a.bottom)
}


SMM.caluculateNewChildPos = function (parent, page, dir) {

    var offsetPos = parent;
    var posX0f = offsetPos.left; //- parent.m_selfWidth/2;
    var pos = {
        x: parent.m_x,
        y: parent.m_y
    };


    var hasPoint = true,
        count = 0,
        stepper = 1,
        cy = parent.m_selfWidth * 1.9,
        cx = parent.m_selfHeight * 1.9,
        ax, ay,
        range = [0, 2 * Math.PI],
        type = 0,
        offset, aType = 0;


    offset = new Object();

    if (page == undefined) {
        offset.x = 88;
        offset.y = 33;
    } else {
        offset.x = page.m_selfWidth;
        offset.y = page.m_selfHeight;

    }

    if (parent.m_parentPage != undefined && dir == undefined) {

        //below line is Original COde for calculating angle we are tweeking it
        //  var pt = parent.m_parentPage.getpositionHack();

        var parentPos = parent.m_parentPage;
        var pt = {
            x: parentPos.m_x,
            y: parentPos.m_y
        };

        var angle = (Math.atan2(pt.y - pos.y, pt.x - pos.x)) * 180 / Math.PI;

        var diff = 0;

        angle = 180 + angle;

        if (angle == 0 || angle == 360)
            angle = 0;

        range.length = 0;
        range = caluculateRange(angle, range, diff);


    } else if (dir != null) {

        range.length = 0;
        range = caluculateRange1(range, dir);
    }

    while (hasPoint) {

        type = (type == 0) ? 1 : 0;

        if (count > 0) {

            if (type == 0) {

                cx = ((Math.sqrt(3) / 2 * ay) + 45);
                cy = (Math.sqrt(3) / 2 * ax) * 1.9;
            } else {
                cx = ((Math.sqrt(3) / 2 * ax) + 45);
                cy = (Math.sqrt(3) / 2 * ay) * 1.9;

            }

            type = (type == 0) ? 1 : 0;


            if (parent.m_parentPage) {
                aType = 1;

                if (count > 3) aType = 2;
            } else
                aType = 0;

        }

        ax = cx;
        ay = cy;

        var pos1 = SMM.getelipsePoint(pos, parent, cx, cy, range, aType, offset);
        hasPoint = pos1 ? false : true;

        count++;
    }


    if (!hasPoint) {
        return pos1;
    } else {
        pos1 = undefined;
        return undefined;
    }

}


SMM.validateDragPage = function () {

    if (draggedShape != undefined && nearestShape != undefined) {

        var isValid = SMM.hasValidDraggedLocation(draggedShape, nearestShape);

        if (!isValid) {

            nearestShape.resetStyle();
            nearestShape.draw();
            return;
        }


        var index = draggedShape.m_parentPage.m_children.indexOf(draggedShape);
        draggedShape.m_parentPage.m_children.splice(index, 1);


        nearestShape.resetStyle();

        var index1 = arGlobalNodes.indexOf(draggedShape);
        arGlobalNodes.splice(index1, 1);



        SMM.addNewPage(nearestShape, draggedShape, index, 'dragged');



        curEbag.layout();

        isreCaluculate = false;
        SMM.setNewPosition(draggedShape, 'dragged');
        isreCaluculate = true;



    } else if (draggedShape != undefined) {

        var index1 = arGlobalNodes.indexOf(draggedShape);
        arGlobalNodes.splice(index1, 1);
        arGlobalNodes.push(draggedShape);

    }

    // draggedShape.parentEbag.m_currPageBeingDragged.m_levelId = draggedShape.m_parentPage.m_levelId + 1;
}

SMM.hasValidDraggedLocation = function () {
    var isValid = true;
    if (draggedShape.m_parentPage == nearestShape) isValid = false;
    if (nearestShape.m_parentPage == draggedShape) isValid = false;
    if (isValid == true)
        checkRelationShip(draggedShape, nearestShape);
    var hasRelation = false;

    function checkRelationShip(draggedPage, nearestPage) {
        //console.log(draggedPage.m_textFormat.m_value +' ::::: '+nearestPage.m_textFormat.m_value);
        if (draggedPage == nearestPage) {
            isValid = false;
            hasRelation = true;
            //  alert( draggedPage.m_textFormat.m_value +'::'+isValid+'::'+nearestPage.m_textFormat.m_value +'Are Same...No..');
            return;
        } else {
            hasRelation = false;
            //  isValid = true;
        }
        draggedPage.m_children.forEach(function (page) {
            if (hasRelation == false)
                checkRelationShip(page, nearestPage);
        })

    }


    return isValid;
}


function getMaxLevelID(page, maxLevel) {

    if (page.m_children.length > 0) {
        maxLevel++;
        page.m_children.forEach(function (page) {

            getMaxLevelID(page, maxLevel);
        });

    }

}


SMM.setNewPosition = function (parentPage, isDragged) {

    parentPage.m_children.forEach(function (page) {

        var index = parentPage.m_children.indexOf(page);
        parentPage.m_children.splice(index, 1);

        if (isDragged != undefined) {
            var index1 = arGlobalNodes.indexOf(page);
            arGlobalNodes.splice(index1, 1);
        }

        SMM.addNewPage(parentPage, page, index, isDragged);
        curEbag.layout();

        SMM.setNewPosition(page, isDragged);

    });

    //var val = parentPage.m_parentPage.m_strNodeShapeFill;
    //changebackGroundColorForPage(parentPage,val);

}




SMM.getelipsePoint = function (center, parent, ax, by, range, type, offset) { //In degrees

    var px, py, hasPoint = true,
        point;

    for (var i = range[0]; i < range[1] + 0.1;) {

        px = center.x - (ax * Math.sin(i)) * Math.sin(0 * Math.PI) + (by * Math.cos(i)) * Math.cos(0 * Math.PI);
        py = center.y + (by * Math.cos(i)) * Math.sin(0 * Math.PI) + (ax * Math.sin(i)) * Math.cos(0 * Math.PI);

        var rect1 = {
            left: px - offset.x - 6,
            top: py - offset.y - 6,
            right: px + offset.x + 6,
            bottom: py + offset.y + 6
        }



        var shape = SMM.hitTestShape(rect1);

        rect1 = undefined;

        hasPoint = shape ? true : false;

        if (!hasPoint) {
            point = new Point(px, py);
            break;
        }


        switch (type) {

            case 0:
                i = i + Math.PI / 4;
                break;
            case 1:
                i = i + Math.PI / 8;
                break;
            case 2:
                i = i + Math.PI / 12;
                break;

        }


    }

    return point;
}



function getFocii(ax, by) {

    var value = 0,
        mx = Math.pow(ax, 2),
        my = Math.pow(by, 2);

    value = Math.sqrt(mx - my);

    return value;

}



function caluculateRange(angle, range, diff) {

    if (angle <= 45 || angle >= 315) { //S1

        if (angle <= 45)
            diff = Math.PI / 4 * angle / 45;
        else
            diff = -Math.PI / 4 * angle / 315;


        range[0] = -Math.PI / 4 + diff;
        range[1] = Math.PI / 4 + diff;

    } else if (angle > 45 && angle <= 135) { //S2

        if (angle <= 90)
            diff = -Math.PI / 4 * (90 - angle) / 45;
        else
            diff = Math.PI * 3 / 4 * (angle - 90) / 135;

        range[0] = Math.PI / 4 + diff;
        range[1] = Math.PI * 3 / 4 + diff;

    } else if (angle > 135 && angle <= 225) { //S3

        if (angle <= 180)
            diff = -Math.PI * 3 / 4 * (180 - angle) / 135;
        else
            diff = Math.PI * 5 / 4 * (angle - 180) / 225;

        range[0] = Math.PI * 3 / 4 + diff;
        range[1] = Math.PI * 5 / 4 + diff;

    } else if (angle > 225 && angle < 315) { //S4

        if (angle <= 270)
            diff = -Math.PI * 5 / 4 * (270 - angle) / 225;
        else
            diff = Math.PI * 7 / 4 * (angle - 270) / 315;


        range[0] = Math.PI * 5 / 4 + diff;
        range[1] = Math.PI * 7 / 4 + diff;

    }


    return range;
}

function caluculateRange1(range, type) {
    

    if (type == 1) { //S1
        range[0] = -Math.PI / 2 + 0.1;
        range[1] = 0;

    } else if (type == 2) { //S2

        range[0] = 0.1;
        range[1] = Math.PI / 2;

    } else if (type == 3) { //S3


        range[0] = Math.PI / 2 + 0.1;
        range[1] = Math.PI;

    } else if (type == 4) { //S4

        range[0] = Math.PI + 0.1;
        range[1] = Math.PI + (Math.PI / 2) - 0.1;

    }


    return range;
}


Math.radians = function (deg) {
    return deg * (Math.PI / 180);
}

var angle;
SMM.caluculatePageOrder = function (parentPage) {

    //Get angle and Put it in appropriate Position
    curEbag.m_iNumberOfRightNodes = 0;

    arrRightTop.length = 0;
    arrRightBottom.length = 0;
    arrLeftBottom.length = 0;
    arrLeftTop.length = 0;



    parentPage.m_children.forEach(function (page) {
        

        if (page.m_parentPage.m_parentPage == undefined) {

            angle = SMM.slopeFromParent(page);
            //getAngle(parentPage.m_group.getpositionHack(),page.m_group.getpositionHack());

            page.angle = angle;


            if (angle > 90 && angle <= 180) {
                //console.log(page.m_textFormat.m_strNode_text + " is in Q1");
                page.m_iIsAboveCenter = 1;
                curEbag.m_iNumberOfRightNodes++;
                arrRightTop.push(page);

            } else if (angle > -180 && angle < -90) {
                //    console.log(page.m_textFormat.m_strNode_text + " is in Q2");
                page.m_iIsAboveCenter = 0;
                curEbag.m_iNumberOfRightNodes++;
                arrRightBottom.push(page);
            } else if (angle >= -90 && angle < 0) {
                //  console.log(page.m_textFormat.m_strNode_text + " is in Q3");
                page.m_iIsAboveCenter = 0;
                arrLeftBottom.push(page);
            } else if (angle >= 0 && angle <= 90) {
                //   console.log(page.m_textFormat.m_strNode_text + " is in Q4");
                page.m_iIsAboveCenter = 1;
                arrLeftTop.push(page);
            }

        }

    });
    //
    SMM.reArrangeRootChildren(parentPage, 0);


}


function getAngle(pt2, pt1) {
    return Math.atan2(pt2.y - pt1.y, pt2.x - pt1.x) * 180 / Math.PI;

}