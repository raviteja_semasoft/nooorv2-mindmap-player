String.prototype.nl2br = function () {
    return this.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
}


SMM.showUiLoader = function (value, text) {

    if (value) {
        $('#uiLoader').css('display', 'block');
        if (text)
            $("#uiLoaderTxt").text('Calculating Positions');
    } else {
        $('#uiLoader').css('display', 'none');
        $("#uiLoaderTxt").text('Calculating Positions');
    }

}

SMM.showLoader = function (value, text) {


    if (value) {
        $('#loaderDiv').css('display', 'block');
        $('#loader').css('display', 'block');
        //  $(".panel").addClass("opacity");

        if (text)
            $("#loaderTxt").text(text);
    } else {
        $('#loader').css('display', 'none');
        $('#loaderDiv').css('display', 'none');
        $("#loaderTxt").text('');
    }
}


SMM.hideColorPicker = function (value) {

    $('#footerColorPallette').hide();
    if (!value)
        $('#colorPickerCon').hide();
    else
        $('#colorPickerCon').hide();

}




SMM.showLoaderDiv = function (value, text) {
    if (text) text = text;
    else text = 'Loading..';
    if (value) {
        $('#loaderDiv').show();
        $('#loaderTxt').html(text);
    } else {
        $('#loaderDiv').hide();
        $('#loaderTxt').html(text);
    }
}

//Toggle Multimedia selections
function toggleMultiMedia(thisPage) {
    if (thisPage == undefined) return;

    if (thisPage.m_strMultimediaUrl.length > 0) {
        // return;
        var mediaType = thisPage.m_strMultimediaType;
        $('#multiMediaUpaloadType input[name="multimediatype"]').each(function ($this) {
            var val = $(this).val();
            if (val == thisPage.m_strMultimediaType) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }

        });

        if (mediaType == 'Image') {
            var url = thisPage.m_strMultimediaUrl;
            var isEmbed = url.substr(url.length - 5);
            var isImgNode = url.substr(url.length - 7);

            $('.isEmbedOrAttachment').fadeIn();
            $('#wInput').empty();
            $('#hInout').empty();
            if (isEmbed == 'Embed') {
                $('#embedAsRadio').prop('checked', true);
                $('#imageAsNodeRadio').prop('checked', false);
                $('#attachmentNodeRadio').prop('checked', false);
                $('#wInput').val(thisPage.m_iImageWidth);
                $('#hInout').val(thisPage.m_iImageHeight);
            } else if (isImgNode == 'ImgNode') {
                $('#embedAsRadio').prop('checked', false);
                $('#imageAsNodeRadio').prop('checked', true);
                $('#attachmentNodeRadio').prop('checked', false);
                $('#wInput').val(thisPage.m_iImageWidth);
                $('#hInout').val(thisPage.m_iImageHeight);

            } else {
                $('#embedAsRadio').prop('checked', false);
                $('#imageAsNodeRadio').prop('checked', false);
                $('#attachmentNodeRadio').prop('checked', true);
            }
        } else {
            $('.isEmbedOrAttachment').fadeOut();

        }

    }
}


function sleep(millis, callback, param) {
    setTimeout(function () {
        callback(param);
    }, millis);
}

//Relocate new URL when clicking on hyperlink
function relocateToURL(url) {
    if (url) {
        var pattern = /^((http|https|ftp):\/\/)/;

        if (!pattern.test(url)) {
            url = "http://" + url;
            window.open(url, '_blank');
        } else {
            console.log(url);
            try {
                window.open(url, '_blank');
            } catch (e) {
                alertBox(e.description);
            }

        }
    }
}


SMM.showStatusMessages = function (msg) {
    if (msg == undefined)
        msg = 'Status';

    $('#ebagStatusDiv label').html(msg);
    $('#ebagStatusDiv').show();
    setTimeout(function () {
        $("#ebagStatusDiv").hide();
    }, 2500);
}

//For Getting Task Test
function getTaskText(node) {
    var arr = [];

    if (node.m_strStartDate.length > 0)
        arr.push('Start Date :' + node.m_strStartDate);
    if (node.m_strDueDate.length > 0 && node.m_strStartDate.length > 0)
        arr.push('End Date :' + node.m_strDueDate);
    if (node.m_strResourceType.length > 0)
        arr.push('Resource :' + node.m_strResourceType);
    if (node.m_iResourceDuration > 0)
        arr.push('Duration :' + node.m_iResourceDuration + ' ' + node.m_strDurationType);


    var text = '';
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].length > 0)
            text += arr[i] + '<br />'

    }
    return text;
}

function shadeColor(color, percent) {
    var R = parseInt(color.substring(1, 3), 16);
    var G = parseInt(color.substring(3, 5), 16);
    var B = parseInt(color.substring(5, 7), 16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R < 255) ? R : 255;
    G = (G < 255) ? G : 255;
    B = (B < 255) ? B : 255;

    var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
    var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
    var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

    return "#" + RR + GG + BB;
}

//When dragging stage layer
SMM.stageMove = function () {

    $('#popover').fadeOut();
    $('#notesDiv').fadeOut();
    $('#fontStylesPopOverContent').hide();
    console.log('Stage Moving');
}

SMM.showFooterColorPicker = function () {

    isFooterColor = true;

    var offset = $('#notesColorPicker').offset();
    $('.slider').css('margin-left', '0px');
    $('.myrange').val(0);
    var bgcLeft = offset.left - 20;
    $('#footerColorPallette').css({
        left: bgcLeft + 'px',
        top: offset.top - 180 + 'px',
        position: 'fixed'
    });

    // bgcLeft += 5;
    var desination = $('#footerColorPallette').offset();
    $('#colorPickerCon').css({
        left: desination.left + 10,
        top: desination.top + 10,
        position: 'fixed',
        'z-index': '99999'
    }).fadeIn("slow");
}

SMM.hideFooterColorPicker = function () {
    SMM.hideColorPicker();
    $('#footerColorPallette').css('display', 'none');

}

//Panning related functionality

$('#min').click(function (event) {

    $('#irange').val($('#irange').val() - 0.1);
    zoomingEbag($('#irange').val());
});

$('#max').click(function (event) {
    $('#irange').val(Number($('#irange').val()) + 0.1);
    zoomingEbag($('#irange').val());
});


SMM.createPanDiv = function () {
    var node = document.getElementById('mainDiv');
    //$('#panCon').empty();
    //html2canvas(element, {
    //    onrendered: function(canvas) {
    //     var url = canvas.toDataURL();
    //        console.log(url);
    //        $('#panCon').empty();
    //       $('#panCon').append('<img style="width:150px;height:150px" src='+url+'>');
    //
    //    },
    //    width: 150,
    //    height: 150
    //});



    domtoimage.toPng(node)
        .then(function (dataUrl) {
            var img = new Image();
            img.src = dataUrl;
            $('#panCon').empty();
            $('#panCon').append(img);
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}

$('.imgContainer').on('click', function (evt) {
    evt.preventDefault();
    evt.stopPropagation();
    //alert($(this).data());
});

$('#irange').on('change', function (evt) {
    evt.preventDefault();
    SMM.EBag().m_scale = $(this).val();
    var cont = document.getElementById('container');
    cont.style.transform = "scale(" + SMM.EBag().m_scale + ")";
});

$("#btnScreenShot").click(function () {

    fitToScreen();
    $('#sideContent').hide();
    $('#slideButton').hide();
    SMM.hideMainMenu(true);
    html2canvas($("#mainDiv"), {
        useCORS: true
    }).then(function (canvas) {
        $('#sideContent').show();
        $('#slideButton').show();

        var img = canvas.toDataURL("image/jpeg");
        img = img.replace('data:image/jpeg;base64,', '');
        var finalImageSrc = 'data:image/jpeg;base64,' + img;
        window.open(finalImageSrc);


    });
});




//Fit To Screen

function fitToScreen() {
    $('#container').panzoom("reset", {
        animate: false,
        contain: false
    });
    SMM.EBag().caluculateLayout(SMM.EBag().m_pages[0]);
    var width = maxX - minX,
        height = maxY - minY + 100;

    var displayWidth = $('#mainDiv').width(),
        displayHeight = $('#mainDiv').height();
    var rootNode = SMM.EBag().m_pages[0];


    var scale = 1,
        scaleX = 1,
        scaleY = 1;
    scaleX = displayWidth / width;
    scaleY = displayHeight / height;
    scale = Math.min(scaleX, scaleY);
    var offSetX, offSetY;
    //   $('#container').panzoom("zoom", scale, { silent: true });

    //Center Position with scale
    var pt = new Object();
    pt.x = (maxX + minX) / 2;
    pt.y = (minY + maxY) / 2;

    //Get relative offset from the root Node

    var pos2 = rootNode.getPosition();
    //pos2.left += ( rootNode.m_selfWidth/2  / scale );
    //pos2.top  += ( rootNode.m_selfHeight/2 / scale);

    //Calculating offset with respect to center and root node position to translate stage
    offSetX = (pos2.left - pt.x) * scale,
        offSetY = (pos2.top - pt.y) * scale;

    $('#container').panzoom("setMatrix", [scale, 0, 0, scale, offSetX ? offSetX : 0, offSetY ? offSetY : 0], {
        animate: true
    });

}


function updateStageDrag(pt, scale) {

    var x = pt.x / scale,
        y = pt.y / scale;

    var offset = 0;


    $('#container').panzoom("setMatrix", [scale, 0, 0, scale, x, y]);

}
//Actual Size


function initStageDrag() {

    $('#container').panzoom("reset", {
        animate: false,
        contain: false
    });

}

function printdiv() {


    fitToScreen();
    var width = maxX - minX,
        height = maxY - minY + 39;
    var scale = SMM.EBag().m_scale;




    window.print();

}




//Jquery Position Hack to get scaled position

$.fn.positionHack = function () {
    var t = $(this),
        p = t.position();
    var scale = SMM.EBag().m_scale;
    var factor = 1 / scale;

    if (scale != 1) {
        if (scale < 1) {
            p.left = Math.ceil(p.left / scale);
            p.top = Math.ceil(p.top / scale);
        } else if (scale > 1) {
            p.left = Math.ceil(p.left * factor);
            p.top = Math.ceil(p.top * factor);
        }
    }
    return p;
};


function loadMultiMediaContent(page, img, isEmbed) {
    //hiding the node text popover.
    $('#popover').css('display', 'none');
    var source = page.m_strMultimediaUrl,
        appendDiv = $('#muilMediaDivContentBody');

    appendDiv.empty();
    var content;
    var temp = source.slice(-3);

    $('#fullScreenBtn').css('display', 'block');
    var mediaType = page.m_strMultimediaType;

    switch (mediaType) {
        case 'Audio':
            $('#fullScreenBtn').css('display', 'none');
            content = '<audio controls="controls"  id="audioPlayer">' +
                ' <source src=' + source + ' preload="auto" type="audio/mp3"  onerror="fileNotFoundError()">' +
                '</audio>';
            break;
        case 'Video':

            content = '<video  controls="controls">' +
                '<source preload="auto" src="' + source + '" type="video/mp4" onerror="fileNotFoundError()">' +
                '</<video>';


            break;
        case 'Image':

            content = '<img src=' + source + ' class="center-block" onerror="fileNotFoundError()">';


            break;
        case 'swf':
            content = '<object width="240px" height="190px">' +
                '<param name="movie" value=' + source + '>' +
                '<embed src=' + source + ' > </embed>' +
                '</object>';
            break;


    }
    var val = 120;
    if (isEmbed) val = 80;

    showMultimediaDiv(page, img);



    if (content) {
        $(content).appendTo(appendDiv);
    }


}


function showMultimediaDiv(page, img) {



    var absY, absX;
    var scale = page.parentEbag.m_scale;

    var nodePos = $(img).offset();

    nodePos.top = nodePos.top - 65;

    absY = nodePos.top;
    absX = nodePos.left + (page.m_selfWidth * scale) / 2 - 125;


    if (absY > 225) {
        absY -= 200;
        //remove top tail div
        //add bottom tail Div
        $('#multiMediaDiv').removeClass('toptailDiv');
    } else {
        absY += (page.m_selfHeight * scale) + 65;
        //remove top tail div
        //add bottom tail Div
        $('#multiMediaDiv').addClass('toptailDiv');
    }

    $('#multiMediaDiv').show()
        .css({
            display: 'block',
            top: absY,
            left: absX,
            position: 'fixed'
        });

}


SMM.displayNotes = function ($this, notes) {

    $('#multiMediaDiv').hide();
    $('#muilMediaDivContentBody').empty();

    var notes = notes;

    var appendTextArea = $('#popOverContent');



    $('#popOverContent').html('');

    $('#popOverContent').html(notes);




    $('#popover').fadeIn();
    var scale1 = 2;

    var dirNotes = $('#notesTextArea').css('direction');


    $('#popOverContent').css('height', 'auto');
    var scrollHeight = $('#popOverContent').prop('scrollHeight');
    //  console.log(scrollHeight);
    if (scrollHeight > 160) {
        $('#popOverContent').css('height', '160px');
    } else {
        var ht = scrollHeight + 'px';
        $('#popOverContent').css('height', ht);
    }

    var hgt = $('#popover').height();
    var wgt = $('#popover').width();
    var pos = $($this).offset();

    $('#popover').css({
        top: pos.top - hgt - 5,
        left: pos.left - wgt / 2 + 8
    });


}



$('.closeArrow').on('click', function (evt) {
    evt.preventDefault();
    $(this).parent().hide();

});




$('#fullScreenBtn').on('click', function (evt) {
    evt.preventDefault();
    $('#muilMediaDivContent').toggleClass('fullScreenDiv');
});

$('#closeFullScreenDiv').on('click', function (evt) {
    evt.preventDefault();
    $('#muilMediaDivContent').toggleClass('fullScreenDiv');
});