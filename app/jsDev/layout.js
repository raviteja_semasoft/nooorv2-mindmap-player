var nodeCount = 0,
    animEndCount = 0,
    arGlobalNodes = [],
    arGlobal = [];
SMM.layout = function () {
    var layout = SMM.EBag().m_iMapLayout;
    console.log(layout);
    switch (parseInt(layout)) {

        case 0:
            SMM.circularLayout();
            break;
        case 3:
            SMM.freeMapLayout();
            break;
        case 2:
            SMM.RightLayout();
            break;
        case 1:
            SMM.LeftLayout();
            break;

        default:
            SMM.circularLayout();
            break;
    }
}


SMM.resetLayout = function () {
    $('#container svg').each(function (index) {
        $(this).remove();
    });

    var rootNode = curEbag.m_pages[0].m_group;
    var w = $(rootNode).outerWidth();
    var h = $(rootNode).outerHeight();
    curEbag.m_pages[0].m_x = $('#container').outerWidth() / 2 - w / 2;
    curEbag.m_pages[0].m_y = $('#container').outerHeight() / 2 - h / 2
    rootNode.style.left = $('#container').outerWidth() / 2 - w / 2 + 'px';
    rootNode.style.top = $('#container').outerHeight() / 2 - h / 2 + 'px';

}


SMM.resetLineArray = function () {

    arGlobal.forEach(function (ar) {
        ar.length = 0;
        ar = undefined;
    });

    arGlobal.length = 0;

    $('#container svg').each(function (index) {

        $(this).remove();
    });

}


SMM.setPositions = function (parentPage, pages, collapse) {
    //console.log('Parent page '+parentPage.m_textFormat.m_strNode_text);

    var curEbag = SMM.EBag();
    var arrPoints = [];
    var draggedLayerPosition = -1;
    for (var i = 0; i < pages.length; i++) {
        var pg = pages[i];
        // console.log('Set Positions '+pg.m_textFormat.m_strNode_text+' :    '+pg.m_x );
        if (pg != curEbag.m_currPageBeingDragged) {
            if (parentPage.m_isCollapsed == true || collapse == true) {
                $(pg.m_group).hide();
                SMM.setPositions(pg, pg.m_children, true);

            } else {
                $(pg.m_group).show();

                var width = pg.m_selfWidth / 2;
                var height = pg.m_selfHeight / 2;
                pg.setPosition(pg.m_x, pg.m_y);
                arrPoints.push(pg.m_x + width);
                arrPoints.push(pg.m_y + height);
                SMM.setPositions(pg, pg.m_children, false);

            }


        } else {
            draggedLayerPosition = i * 2;

            SMM.setPositions(pg, pg.m_children);
            arrPoints.push(pg.m_x);
            arrPoints.push(pg.m_y + 5);
        }



    }
    var parentWidth = $(parentPage.m_group).outerWidth() / 2;
    var parentHeight = $(parentPage.m_group).outerHeight() / 2;
    // parentPage.setPosition({x:parentPage.m_x+parentWidth, y:parentPage.m_y});

    parentPage.setPosition(parentPage.m_x, parentPage.m_y);
    var xFrom = parentPage.m_x + parentWidth;
    var yFrom = parentPage.m_y + parentHeight;

    for (var j = 0; j < arrPoints.length; j = j + 2) {

        if (draggedLayerPosition != j) {

            var sign = ((xFrom > arrPoints[j]) ? 1 : -1);
            var lpoint = new SMM.SLine2();
            lpoint.clr = parentPage.m_strNodeShapeFill;
            lpoint.moveTo(xFrom, yFrom);

            var cp1x = xFrom,
                cp1y = (arrPoints[j + 1] + yFrom) / 2,
                cp2x = arrPoints[j],
                cp2y = (arrPoints[j + 1] + yFrom) / 2,
                x = arrPoints[j],
                y = arrPoints[j + 1];

            lpoint.bCurveto(cp1x, cp1y, cp2x, cp2y, x, y);
            arGlobal.push(lpoint);
        }
    }


}








SMM.getDistance = function (page) {

    var pos1 = $(page.m_group).position(),
        pos2 = $(curEbag.m_currPageBeingDragged.m_group).position();
    var pageXPos = pos1.left;
    var pageYPos = pos1.top;
    var currPageXPos = pos2.left;
    var currPageYPos = pos2.top;
    return Math.sqrt(((pageXPos + page.m_selfWidth / 2 - currPageXPos - curEbag.m_currPageBeingDragged.m_selfWidth / 2) *
            (pageXPos + page.m_selfWidth / 2 - currPageXPos - curEbag.m_currPageBeingDragged.m_selfWidth / 2)) +
        ((pageYPos + page.m_selfHeight / 2 - currPageYPos - curEbag.m_currPageBeingDragged.m_selfHeight / 2) *
            (pageYPos + page.m_selfHeight / 2 - currPageYPos - curEbag.m_currPageBeingDragged.m_selfHeight / 2))
    );
}

SMM.animateLayout = function () {
    nodeCount = document.querySelectorAll('.node').length - 1;
    animEndCount = 0;
    var parent = curEbag.m_pages[0];

    animateChild(parent);

    function animateChild(parent) {

        parent.m_children.forEach(function (page) {
            page.animateNode();
            animateChild(page);
        });

    }
    isAnimate = false;
}



SMM.generatePageLoc = function () {

    SMM.updateNodePositions();
    SMM.updateStrLocation(curEbag.m_pages[0], arrRightTop);
    SMM.updateStrLocation(curEbag.m_pages[0], arrRightBottom);
    SMM.updateStrLocation(curEbag.m_pages[0], arrLeftTop);
    SMM.updateStrLocation(curEbag.m_pages[0], arrLeftBottom);

}

SMM.updateStrLocation = function (parentPage, pages) {

    for (var i = 0; i < pages.length; i++) {
        if (parentPage.m_parentPage != undefined)
            pages[i].m_strLocation = parentPage.m_strLocation + '/' + i;

        //Provide validation for collapse
        SMM.setPositionsRightMap(pages[i], pages[i].m_children);
    }
}

SMM.initLayout = function () {

    var parent = curEbag.m_pages[0];
    switch (curEbag.m_iMapLayout) {
        default: SMM.reArrangeChildPos(parent);
        break;
        case 1:
                SMM.reCaluculatePagePosition(parent);
            break;

    }

}


SMM.distanceFromParent = function (page) {

    var pt1 = page.m_parentPage.getPosition();
    var pt2 = page.getPosition();
    var xs = 0,
        ys = 0;

    xs = pt2.x - pt1.x;
    xs = xs * xs;

    ys = pt2.y - pt1.y;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
}


SMM.slopeFromParent = function (page) {

    var pt1 = $(page.m_group).positionHack();
    var pt2 = $(page.m_parentPage.m_group).positionHack();
    var xs = 0,
        ys = 0;

    xs = pt2.left - pt1.left;
    xs = xs * xs;

    ys = pt2.top - pt1.top;
    ys = ys * ys;

    return Math.atan2(pt2.top - pt1.top, pt2.left - pt1.left) * 180 / Math.PI;
}


SMM.reArrangeRootChildren = function (parentPage, type) {
    debugger;
    var value = false;
    arrRightTop.sort(sortComparator("m_iNode_Order", value));
    arrRightBottom.sort(sortComparator("m_iNode_Order", value));


    arrLeftTop.sort(sortComparator("m_iNode_Order", value));
    if (type == 1)
        value = true;
    arrLeftBottom.sort(sortComparator("m_iNode_Order", value));

    parentPage.m_children.length = 0;

    parentPage.m_children.push.apply(parentPage.m_children, arrRightTop);
    parentPage.m_children.push.apply(parentPage.m_children, arrRightBottom);
    parentPage.m_children.push.apply(parentPage.m_children, arrLeftBottom);
    parentPage.m_children.push.apply(parentPage.m_children, arrLeftTop);
}

SMM.reCaluculatePagePosition = function (parentPage) {


    SMM.EBag().resetPosition(parentPage);
    SMM.resetLayout();

    //    isreCaluculate  =false;
    function addFirstOrderChildren(arChildren, type) {

        arChildren.forEach(function (_page) {

            var pos = SMM.caluculateNewChildPos(_page.m_parentPage, _page, type);
            _page.setPosition(pos.x, pos.y);
            // _page.draw();
            addChildrentoHigherOrder(_page);
        });


    }


    function addChildrentoHigherOrder(page) {
        page.m_children.forEach(function (_page) {

            var pos = SMM.caluculateNewChildPos(_page.m_parentPage, _page);
            //
            _page.m_x = pos.x;
            _page.m_y = pos.y;
            //
            //_page.setPosition(pos.x,pos.y);
            //_page.draw();
            _page.setPosition(pos.x, pos.y);

            addChildrentoHigherOrder(_page);
        });

    };

    addFirstOrderChildren(arrRightTop, 1);
    addFirstOrderChildren(arrRightBottom, 2);
    addFirstOrderChildren(arrLeftBottom, 3);
    addFirstOrderChildren(arrLeftTop, 4);


    //curEbag.refresh();
    curEbag.layout();

    //    isreCaluculate  =true;
}

function sortComparator(property, reverse) {

    if (reverse == false) {
        return function (a, b) {
            console.log('value...' + a[property] - b[property]);
            return a[property] - b[property];
        }
    } else {
        return function (a, b) {

            return -(a[property] - b[property]);
        }
    }

}



//Draw Cubic Bezier Curve lines in Freemap layout
SMM.drawDashedLine = function () {
    arGlobal.forEach(function (sline) {
        //Dynamically creating Path for SVG Line drawings

        var svgLine = 'M ' + sline.mx + ' ' + sline.my + ' ' + 'C ' + sline.cpx + ' ' + sline.cpy + ' ' + sline.cp1x + ' ' + sline.cp1y + ' ' + sline.qcx + ' ' + sline.qcy + ' ';

        var w = $('#container').outerWidth(),
            h = $('#container').outerHeight();
        $('#container').append($('<svg height=' + h + ' width=' + w + ' style="position: absolute;"> ' +
            '<path stroke-dasharray="5,5" d="' + svgLine + '" stroke="' + sline.clr + '" stroke-width="2" fill="none" />' +
            '</svg>'));

    });

}

//Layout Switching



function layoutCircularCallBack() {
    debugger;
    SMM.showUiLoader(true);
    SMM.caluculatePageOrder(curEbag.m_pages[0]);
    SMM.reArrangeRootChildren(curEbag.m_pages[0], 1);
    SMM.reArrangeChildPos(curEbag.m_pages[0]);
    SMM.animateLayout();
    SMM.showUiLoader(false);

};

function initPositions(parent) {
    parent.setPosition();
    parent.m_children.forEach(function (page) {
        initPositions(page);
    })

}

function confirmMapLayoutChange() {
    $('#layoutChangeConfirmModal').modal();
}

$('#layoutMapperConfirmNoBtn').on('click', function (evt) {
    evt.preventDefault();
    $('#layoutChangeConfirmModal').modal('hide');
    SMM.showUiLoader(true, 'Calculating Positions');
    sleep(0, layoutNoCallBack, "");

});

$('#layoutMapperConfirmYesBtn').on('click', function (evt) {
    evt.preventDefault();
    $('#layoutChangeConfirmModal').modal('hide');
    SMM.showUiLoader(true, 'Calculating Positions');
    sleep(0, layoutYesCallBack, "");

});


SMM.SLine = function () {}

SMM.SLine.prototype.moveTo = function (x, y) {
    this.mx = x;
    this.my = y;
}

SMM.SLine.prototype.qCurveto = function (cpx, cpy, x, y) {

    this.cpx = cpx;
    this.cpy = cpy;
    this.qcx = x;
    this.qcy = y;
}
SMM.SLine.prototype.color = function (clr, type) {
    this.clr = clr;
    this.type = type;
}

SMM.SLine.prototype.lineTo = function (x, y) {
    this.mx1 = x;
    this.my1 = y;
}


SMM.SLine2 = function () {}

SMM.SLine2.prototype.moveTo = function (x, y) {

    this.mx = x;
    this.my = y;

}

SMM.SLine2.prototype.bCurveto = function (cpx, cpy, cp1x, cp1y, x, y) {

    this.cpx = cpx;
    this.cpy = cpy;
    this.cp1x = cp1x;
    this.cp1y = cp1y;
    this.qcx = x;
    this.qcy = y;
}
SMM.SLine2.prototype.color = function (clr) {
    this.clr = clr;
}