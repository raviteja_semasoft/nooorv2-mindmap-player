SMM.LeftLayout = function(){

    SMM.resetLineArray();
    SMM.resetBounds();
    var rootNode =  curEbag.m_pages[0].m_group;
    var h = $(rootNode).outerHeight();
    var rootNodeWidth = $(rootNode).outerWidth();
    var centerX = Math.ceil($('#container').innerWidth()/2 - rootNodeWidth/2);
    var centerY = Math.ceil($('#container').innerHeight()/2 - h/2);
    rootNode.style.left = centerX+'px';
    rootNode.style.top  = centerY+'px';

    SMM.getChildrenHeight(curEbag.m_pages[0], curEbag.m_pages[0].m_children);
    SMM.resetLineArray();
    SMM.updateNodePositionsLeftMap();
    curEbag.m_rightPages.length = 0;
    curEbag.m_leftPages.length = 0;

    //
    //if(isPageLoad == true)
    //    return;


    var yOff_rt=0.0,yOff_rb=0.0;
    var top = $(curEbag.m_pages[0].m_group).positionHack().top;

    SMM.setPositionsLeftMap(curEbag.m_pages[0], arrLeftTop, top - ltHeight - 50  - yOff_rt);
    SMM.setPositionsLeftMap(curEbag.m_pages[0], arrLeftBottom, top + 50 + curEbag.m_pages[0].m_selfHeight -yOff_rb );


    if(curEbag.m_iMapStyle == 2)
        SMM.drawLine();
    else if(curEbag.m_iMapStyle == 3)
        SMM.drawThickerCurve();
    else if(curEbag.m_iMapStyle == 1)
        SMM.drawStraightLine();



    if(curEbag.m_currPageBeingDragged != undefined){
        //Find the nearest page on the side m_currPageBeingDragged is and draw a line to its parent based on relateive x position.
        if(curEbag.m_currPageBeingDragged.m_group.attrs.x + curEbag.m_currPageBeingDragged.m_selfWidth/2 > //for checking whether dragged page is on right or left
            curEbag.m_pages[0].m_group.attrs.x + curEbag.m_pages[0].m_selfWidth/2){


            curEbag.m_nearestPageToTheCurrentPageBeingDragged = curEbag.m_currPageBeingDragged.m_parentPage;

            if(nearestShape != undefined){
                nearestShape.resetStyle();
                nearestShape = undefined;
            }
            nearestShape =   SMM.hitTestShape(curEbag.m_currPageBeingDragged);

            if(nearestShape != undefined)
            {
                console.log('>>>>>>>>>>>>>>>>>IF>>>>>>>>>>>>>>>>>>>');

                if(curEbag.m_currPageBeingDragged.m_parentPage != nearestShape){
                    if((nearestShape.m_parentPage == undefined) ||
                        (nearestShape.m_parentPage.m_parentPage != undefined ) )
                    {

                        nearestShape.highlight();
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    } else
                    {
                        nearestShape.highlight();
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    }
                }


                console.log(nearestShape.m_textFormat.m_strNode_text);
            }

        }else{

            curEbag.m_nearestPageToTheCurrentPageBeingDragged = curEbag.m_currPageBeingDragged.m_parentPage;
            //   console.log('>>>>>>>>>>>>>>>>else part>>>>>>>>>>>>>>>>');
            if(nearestShape != undefined){
                nearestShape.resetStyle();
                nearestShape = undefined;
            }
            nearestShape =   SMM.hitTestShape(curEbag.m_currPageBeingDragged);

            if(nearestShape != undefined)
            {
                if(curEbag.m_currPageBeingDragged.m_parentPage != nearestShape){

                    if((nearestShape.m_parentPage == undefined) ||
                        (nearestShape.m_parentPage.m_parentPage != undefined ) )
                    {

                        nearestShape.highlight();
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    } else
                    {
                        nearestShape.highlight();
                        curEbag.m_nearestPageToTheCurrentPageBeingDragged = nearestShape;
                    }

                }



            }
        }
    }

}


SMM.updateNodePositionsLeftMap = function(){

    arrRightTop.length = 0;
    rtHeight = 0;
    arrRightBottom.length = 0 ;
    rbHeight = 0;
    arrLeftBottom.length = 0 ;
    lbHeight = 0;
    arrLeftTop.length = 0;
    ltHeight = 0;

    var ct = 0;

    var rootNode = curEbag.m_pages[0];
    while(rootNode.m_children.length > ct  &&rootNode.m_children[ct] &&rootNode.m_children[ct].m_iIsAboveCenter == 1){
        arrLeftTop.push(curEbag.m_pages[0].m_children[ct]);
        ltHeight +=rootNode.m_children[ct].m_childrenHeight;
       rootNode.m_children[ct].m_strLocation = '' + (ct );
        ct++;
    }

    while(rootNode.m_children.length > ct  &&rootNode.m_children[ct] &&rootNode.m_children[ct].m_iIsAboveCenter == 0){
        arrLeftBottom.push(curEbag.m_pages[0].m_children[ct]);
        lbHeight +=rootNode.m_children[ct].m_childrenHeight;
       rootNode.m_children[ct].m_strLocation = '' + (ct );
        ct++;
    }
    while(rootNode.m_children.length > ct  &&rootNode.m_children[ct] &&rootNode.m_children[ct].m_iIsAboveCenter == 1){
        arrLeftTop.push(curEbag.m_pages[0].m_children[ct]);
        ltHeight +=rootNode.m_children[ct].m_childrenHeight;
       rootNode.m_children[ct].m_strLocation = '' + (ct );
        ct++;
    }

    while(rootNode.m_children.length > ct  &&rootNode.m_children[ct] &&rootNode.m_children[ct].m_iIsAboveCenter == 0){
        arrLeftBottom.push(curEbag.m_pages[0].m_children[ct]);
        lbHeight +=rootNode.m_children[ct].m_childrenHeight;
       rootNode.m_children[ct].m_strLocation = '' + (ct );
        ct++;
    }


}




















