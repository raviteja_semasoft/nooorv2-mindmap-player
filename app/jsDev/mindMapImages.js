    function mindMapImagesfun() {
        var mindMapImagesobj = {}

        iconImages = [{
                type: "5",
                src: "Images/Icons/8.png",
                urltype: "14",
                name: "image"
            },
            {
                type: "5",
                src: "Images/Icons/7.png",
                urltype: "14",
                name: "sound"
            },
            {
                type: "5",
                src: "Images/Icons/6.png",
                urltype: "14",
                name: "video"
            },
            {
                type: "5",
                src: "Images/Icons/5.png",
                urltype: "14",
                name: "swf"
            },
            {
                type: "5",
                src: "Images/Icons/3.png",
                urltype: "14",
                name: "hyperlink"
            },
            {
                type: "5",
                src: "Images/ajaxload.gif",
                urltype: "14",
                name: "imageLoader"
            }
        ]

        priorityImages = [{
                type: "6",
                src: "Images/PriorityImgs/prior1.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior2.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior3.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior4.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior6.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior7.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior8.png"
            },
            {
                type: "6",
                src: "Images/PriorityImgs/prior9.png"
            },

        ]

        completionImages = [{
                type: "7",
                src: "Images/ComplImgs/completion1.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion2.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion3.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion4.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion5.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion6.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion7.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion8.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion9.png"
            },
            {
                type: "7",
                src: "Images/ComplImgs/ompletion10.png"
            }
        ]

        correctImages = [{
                type: "8",
                src: "Images/correctImgs/correct1.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct2.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct3.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct4.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct5.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct6.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct7.png"
            },
            {
                type: "8",
                src: "Images/correctImgs/correct8.png"
            }
        ]

        clipartsImages = [{
                type: "9",
                src: "Images/clipArts/cliparts1.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts2.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts3.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts4.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts5.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts6.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts7.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts8.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts9.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts10.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts11.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts12.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts13.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts14.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts15.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts16.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts17.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts18.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts19.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts20.png"
            },
            {
                type: "9",
                src: "Images/clipArts/cliparts21.png"
            }
        ]

        smileImages = [{
            type: "10",
            src: "Images/smiley/smile1.png"
        }, {
            type: "10",
            src: "Images/smiley/smile12.png"
        }, {
            type: "10",
            src: "Images/smiley/smile3.png"
        }, {
            type: "10",
            src: "Images/smiley/smile4.png"
        }, {
            type: "10",
            src: "Images/smiley/smile5.png"
        }, {
            type: "10",
            src: "Images/smiley/smile6.png"
        }, {
            type: "10",
            src: "Images/smiley/smile7.png"
        }, {
            type: "10",
            src: "Images/smiley/smile8.png"
        }, {
            type: "10",
            src: "Images/smiley/smile9.png"
        }, {
            type: "10",
            src: "Images/smiley/smile10.png"
        }, {
            type: "10",
            src: "Images/smiley/smile11.png"
        }, {
            type: "10",
            src: "Images/smiley/smile12.png"
        }, {
            type: "10",
            src: "Images/smiley/smile13.png"
        }, {
            type: "10",
            src: "Images/smiley/smile14.png"
        }, {
            type: "10",
            src: "Images/smiley/smile15.png"
        }, {
            type: "10",
            src: "Images/smiley/smile16.png"
        }, {
            type: "10",
            src: "Images/smiley/smile17.png"
        }, {
            type: "10",
            src: "Images/smiley/smile18.png"
        }, {
            type: "10",
            src: "Images/smiley/smile19.png"
        }, {
            type: "10",
            src: "Images/smiley/smile20.png"
        }]

        notesImages = {
            type: "11",
            src: "Images/notes.png"
        };

        generalImages = [{
                type: "13",
                src: "Images/General/Gen1.png"
            },
            {
                type: "13",
                src: "Images/General/Gen2.png"
            },
            {
                type: "13",
                src: "Images/General/Gen3.png"
            },
            {
                type: "13",
                src: "Images/General/Gen4.png"
            },
            {
                type: "13",
                src: "Images/General/Gen5.png"
            },
            {
                type: "13",
                src: "Images/General/Gen6.png"
            },
            {
                type: "13",
                src: "Images/General/Gen7.png"
            },
            {
                type: "13",
                src: "Images/General/Gen8.png"
            },
            {
                type: "13",
                src: "Images/General/Gen9.png"
            },
            {
                type: "13",
                src: "Images/General/Gen10.png"
            },
            {
                type: "13",
                src: "Images/General/Gen11.png"
            },
            {
                type: "13",
                src: "Images/General/Gen12.png"
            },
            {
                type: "13",
                src: "Images/General/Gen13.png"
            },
            {
                type: "13",
                src: "Images/General/Gen14.png"
            },
            {
                type: "13",
                src: "Images/General/Gen15.png"
            },
            {
                type: "13",
                src: "Images/General/Gen16.png"
            },
            {
                type: "13",
                src: "Images/General/Gen17.png"
            },
            {
                type: "13",
                src: "Images/General/Gen18.png"
            },
            {
                type: "13",
                src: "Images/General/Gen19.png"
            },
            {
                type: "13",
                src: "Images/General/Gen20.png"
            },
            {
                type: "13",
                src: "Images/General/Gen21.png"
            },
            {
                type: "13",
                src: "Images/General/Gen22.png"
            }
        ]

        mindMapImagesobj.iconImages = iconImages;
        mindMapImagesobj.correctImages = correctImages;
        mindMapImagesobj.completionImages = completionImages;
        mindMapImagesobj.clipartsImages = clipartsImages;
        mindMapImagesobj.generalImages = generalImages;
        mindMapImagesobj.priorityImages = priorityImages;
        mindMapImagesobj.notesImages = notesImages;
        mindMapImagesobj.smileImages = smileImages;

        return mindMapImagesobj;

    }