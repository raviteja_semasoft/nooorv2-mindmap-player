var arrRightTop = [],
    arrRightBottom = [],
    arrLeftBottom = [],
    arrLeftTop = [];
var rtHeight = 0,
    rbHeight = 0,
    lbHeight = 0,
    ltHeight = 0;

var minX = 0,
    maxX = 0,
    minY = 0,
    maxY = 0,
    arrGLPoints = [];


SMM.EBag = function (id, heigth, mapLayoutType, style, width, mapFolder, noOfRightNodes, parentFolderId, isPublished,
    strAuthor, strComment, strCreatedDate, strMapDescription, strMapLayoutFillColor, strMapProperties, strModifiedDate, tabletPublish, name, refresh) {



    if (refresh)
        arguments.callee._singletonInstance = undefined;

    //Make Singleton
    if (arguments.callee._singletonInstance)
        return arguments.callee._singletonInstance;

    arguments.callee._singletonInstance = this;


    this.m_name = name;
    this.m_iId = id;
    this.m_iMapWidth = width;
    this.m_iMapHeight = heigth;
    this.m_strMapLayoutFillColor = strMapLayoutFillColor;
    this.m_iMapStyle = style ? style : 2;
    this.m_iMaporFolder = mapFolder;
    this.m_iParentFolderId = parentFolderId;
    this.m_isPublished = isPublished;
    this.m_strAuthor = strAuthor;
    this.m_strComment = strComment;
    this.m_strCreatedDate = strCreatedDate;
    this.m_strMapDescription = strMapDescription;
    this.m_strMapProperties = strMapProperties;
    this.m_strModifiedDate = strModifiedDate;
    this.m_strName = name;
    this.m_TabletPublishStatus = tabletPublish;
    this.m_pages = [];
    this.m_iMapLayout = mapLayoutType ? mapLayoutType : 0;
    this.m_currentPage = undefined;
    this.m_isRightNodeBeingMoved = false;
    this.m_addPagePosition = 0;
    this.m_iNumberOfRightNodes = noOfRightNodes;
    this.m_currPageBeingDragged = undefined;
    this.m_nearestPageToTheCurrentPageBeingDragged = undefined;
    this.m_rightPages = [];
    this.m_leftPages = [];
    this.m_scale = 1;
    this.rootOffSet = {
        x: 0,
        y: 0
    }


}



SMM.EBag.prototype.addPage = function (page, direction, at) {
    page.draw();
    var rootNode = page.m_group;
    var h = $(rootNode).outerHeight();
    var rootNodeWidth = $(rootNode).outerWidth();
    var centerX = Math.ceil($('#container').innerWidth() / 2 - rootNodeWidth / 2);
    var centerY = Math.ceil($('#container').innerHeight() / 2 - h / 2);
    rootNode.style.left = centerX + 'px';
    rootNode.style.top = centerY + 'px';
    page.m_x = centerX;
    page.m_y = centerY;
    page.m_levelId = 0;
    this.m_pages.push(page);
    this.layout();
}



SMM.EBag.prototype.layout = function () {
    arrGLPoints.forEach(function (temp) {
        temp.length = 0;
    });
    arrGLPoints.length = 0;
    SMM.resetBounds();
    SMM.layout();

}

SMM.EBag.prototype.caluculatePos = function (root) {

    root.setPosition();
    for (var i = 0; i < root.m_children.length; i++) {
        this.caluculatePos(root.m_children[i]);
    }

}

SMM.EBag.prototype.resetPosition = function (parent) {
    resetPosition(parent.m_children);

    function resetPosition(pages) {
        for (var i = 0; i < pages.length; i++) {
            pages[i].resetPosition();
            if (pages[i].m_children.length > 0) {
                resetPosition(pages[i].m_children);
            }
        }
    }
}


SMM.EBag.prototype.hideAllMenus = function () {
    //commented for testin purpose
    $('#menuFormat').hide();
    $('#popover').hide();
    $('#multiMediaDiv').hide();
    $('#muilMediaDivContentBody').empty();
}


SMM.EBag.prototype.reset = function () {
    arguments.callee._singletonInstance = undefined;
}




SMM.EBag.prototype.caluculateLayout = function (root) {

    root.caluculateBounds();
    for (var i = 0; i < root.m_children.length; i++) {
        this.caluculateLayout(root.m_children[i]);
    }

}





$('#container').on('click', function (evt) {
    evt.preventDefault();

    console.log('StageClicked1');
    $('.nodeTextWrapper').fadeOut();
    // $('#currTextInput').val('');// currTextInput
    $('#footerColorPallette').css('display', 'none');
    SMM.hideColorPicker();
    $('#popover').css('display', 'none');
    $('#nodeTextPopover').css('display', 'none');
    $('#multiMediaDiv').hide();
    $('#muilMediaDivContentBody').empty();
    SMM.EBag().hideAllMenus();
});