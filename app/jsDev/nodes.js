var nodeColor = 0,
    ticking = false,
    transform;
var singleClickCalled = false;
var timer;;
var nodeColor = 0;
var largest, levelArr = [],
    embedImages = [],
    imageAsNodes = [],
    arabic = /[\u0600-\u06FF]/;
SMM.Page = function (ebag, children, iId, mapId, parentId, fx, fy, nodeOrder, boolPinned, priority, completion, strStartDate,
    strDueDate, resourceType, resourceDuration, strDurationType, strMultimediaType, strMultimediaURL, strHyperlinkUrl,
    strHyperlinkDescription, strOpenLinkIn, iSymbol, notes, iIsAttachment, iNodeShape, strNodeShapeFill, iSmileys,
    iFlags, iCheckedStatus, lImage, strShapeLineColor, iShapeLineThickness, fNodeShapeFillColorAlpha, strAttachments,
    iImageHeight, iImageWidth, iBoundary, strBoundaryColor, iType, iGradientType,
    iIsAboveCenter, textFormat) {

    this.parentEbag = ebag;
    this.m_children = children;
    this.m_iId = iId;
    this.m_iMapId = mapId;
    this.m_iParentId = parentId;
    this.m_iNode_Order = nodeOrder;
    this.m_boolPinned = boolPinned;
    this.m_iPriority = priority;
    this.m_iCompletion = completion;
    this.m_strStartDate = strStartDate ? strStartDate : '';
    this.m_strDueDate = strDueDate ? strDueDate : '';
    this.m_strResourceType = resourceType ? resourceType : '';
    this.m_iResourceDuration = resourceDuration ? resourceDuration : 0;
    this.m_strDurationType = strDurationType ? strDurationType : '';
    this.m_strMultimediaType = strMultimediaType ? strMultimediaType : '';
    this.m_strMultimediaUrl = strMultimediaURL ? strMultimediaURL : '';
    this.m_strHyperlinkUrl = strHyperlinkUrl ? strHyperlinkUrl : '';
    this.m_strHyperlinkDescription = strHyperlinkDescription ? strHyperlinkDescription : '';
    this.m_strOpenLinkIn = strOpenLinkIn ? strOpenLinkIn : '';
    this.m_iSymbol = iSymbol;
    this.m_strNode_Notes = notes ? notes : '';
    this.m_iIsAttachment = iIsAttachment;
    this.m_iNodeShape = iNodeShape;
    this.m_strNodeShapeFill = strNodeShapeFill ? strNodeShapeFill : '#CDCDCD';
    this.m_iSmileys = iSmileys;
    this.m_iFlags = iFlags;
    this.m_iCheckedStatus = iCheckedStatus ? iCheckedStatus : 0;
    this.m_lImage = lImage;
    this.m_strShapeLineColor = strShapeLineColor;
    this.m_iShapeLineThickness = iShapeLineThickness;
    this.m_fNodeShapeFillColorAlpha = fNodeShapeFillColorAlpha ? fNodeShapeFillColorAlpha : 1;
    this.m_strAttachments = strAttachments ? strAttachments : '';
    this.m_iImageHeight = iImageHeight;
    this.m_iImageWidth = iImageWidth;
    this.m_iBoundary = iBoundary;
    this.m_strBoundaryColor = strBoundaryColor;
    this.m_iType = iType;
    this.m_iGradientType = iGradientType;
    this.m_iIsAboveCenter = iIsAboveCenter;
    this.m_x = fx;
    this.m_y = fy;
    this.m_parentPage = undefined;
    this.m_styleType = 1;
    this.m_templateId = '';
    this.m_pageId = '';
    this.m_isChild = iIsAboveCenter;
    this.m_pageOrder = 1;
    this.m_textFormat = textFormat ? textFormat : new SMM.TextFormat();
    this.m_strMapLayoutFillColor = '#cccccc'; //this.m_textFormat.m_strNode_TextFillColor;//(bgColor.indexOf('#') >= 0) ? bgColor:'';
    this.m_strLocation = '';
    this.m_tabIndex = -1;
    this.m_children = []; // children;
    this.isCurrent = false;
    this.m_isCollapsed = false;
    this.m_levelId = nodeColor++;
    this.m_isAbove = iIsAboveCenter;

    this.allImages = [];
    this.m_childrenHeight = 0;
    this.m_selfWidth = 0;
    this.m_selfHeight = 0;
    this.m_isDrag = false;
    this.m_largeName = '';
    this.m_drag = false;
    this.bounds = {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    };

    this.attrs = {
        x: 0,
        y: 0
    }

    this.imageWidth = undefined;
    this.imageHeight = undefined;
    this.m_textDirection = undefined;

}

SMM.Page.prototype.setPosition = function (x, y) {



    if (x && y) {
        this.m_x = parseInt(x);
        this.m_y = parseInt(y);
        this.attrs.x = this.m_x;
        this.attrs.y = this.m_y;
        this.m_group.style.left = this.m_x + 'px';
        this.m_group.style.top = this.m_y + 'px';
    } else {
        var pos = $(this.m_group).positionHack();
        this.m_x = pos.left;
        this.m_y = pos.top;
        this.m_group.style.left = this.m_x + 'px';
        this.m_group.style.top = this.m_y + 'px';
        this.attrs.x = this.m_x;
        this.attrs.y = this.m_y;
    }

}

var lineDraw = false;
SMM.Page.prototype.animateNode = function () {

    SMM.resetLineArray();

    var thisPage = this.m_group;

    var pos2 = {
        x: this.m_x,
        y: this.m_y
    };


    var rootNodePos = $(SMM.EBag().m_pages[0].m_group).positionHack();

    var centerX = rootNodePos.left;
    var centerY = rootNodePos.top;

    $(thisPage).css({
        left: centerX + 'px',
        top: centerY + 'px'
    });

    //  console.log(pos2);

    $(thisPage).animate({
        left: pos2.x + 'px',
        top: pos2.y + 'px'
    }, 1000, "linear", function () {
        animEndCount++;
        if (animEndCount == nodeCount) {
            //alert('Animation Completed');
            setTimeout(function () {
                isAnimating = true;
                SMM.layout();
                // document.title = "Nooor Mindmap Player Preview";
                isReorderPosition = false;
                console.log('>>>>>>>>>>>>>>>>>Rendered<<<<<<<<<<<<<<<<<<<<<<<<<<<');
            }, 2500)
        }


    });


}

SMM.Page.prototype.fillShape = function () {
    var node = this.m_group;

    try {
        node.style.backgroundColor = this.m_strNodeShapeFill;
        node.style.borderColor = this.m_strShapeLineColor;
        $(node).data().m_strNodeShapeFill = this.m_strNodeShapeFill;
        $(node).data().m_strShapeLineColor = this.m_strShapeLineColor;

    } catch (err) {
        console.log(err.message);
    }
}


SMM.Page.prototype.getPosition = function () {
    var gPos = $(this.m_group).positionHack();
    return gPos;
}

SMM.Page.prototype.highlight = function () {
    $(this.m_group).addClass('nodeHighlight');
}

SMM.Page.prototype.resetStyle = function () {
    $(this.m_group).removeClass('nodeHighlight');
}

SMM.Page.prototype.resetPosition = function () {

    var centerX = Math.ceil($('#container').outerWidth() / 2 - this.m_selfWidth / 2);
    var centerY = Math.ceil($('#container').outerWidth() / 2 - this.m_selfHeight / 2);
    this.setPosition(centerX, centerY);
}


SMM.Page.prototype.addPage = function (page, at, pos, isDragged) {

    if (at == undefined)
        this.m_children.push(page);
    else
        this.m_children.splice(at, 0, page);

    page.m_parentPage = this;
    if (page.m_parentPage)
        page.m_levelId = page.m_parentPage.m_levelId + 1;
    else
        page.m_levelId = 0;

    if (pos != undefined) {
        page.m_x = pos.x;
        page.m_y = pos.y;
    }

    var isGroupExists = page.m_group;
    if (isDragged == undefined) {
        $('#container').append($(page.m_group));
        page.draw();
    }



    // this.parentEbag.layout();
}

SMM.Page.prototype.getBounds = function () {
    var nodePos = $(this.m_group).positionHack();
    var w = $(this.m_group).outerWidth();
    var h = $(this.m_group).outerHeight();

    this.bounds.left = nodePos.left - w / 2;
    this.bounds.top = nodePos.top - h / 2;
    this.bounds.right = w / 2 + nodePos.left;
    this.bounds.bottom = h / 2 + nodePos.top;
    return this.bounds;
}

SMM.Page.prototype.caluculateBounds = function () {

    var nodePos = $(this.m_group).positionHack();
    var w = $(this.m_group).outerWidth();
    var h = $(this.m_group).outerHeight();

    var x = nodePos.left,
        width = w;
    var y = nodePos.top,
        height = h;
    if (minX == 0 || x < minX) minX = x;
    if (maxX == 0 || x + width > maxX) maxX = x + width;
    if (minY == 0 || y < minY) minY = y;
    if (maxY == 0 || y + height > maxY) maxY = y + height;

}



//Images in Pages

SMM.Page.prototype.loadImages = function () {

    var imgContainer = $(this.m_group).find('.imgContainer');
    if (this.m_iSymbol && this.m_iSymbol > 0) {
        this.allImages = [];
        this.allImages.push(this.m_iSymbol);
        if (this.allImages && this.allImages.length > 0) {
            debugger;
            var imgSrc = clipArtImgs[this.m_iSymbol - 1].src;
            $('<img />', {
                src: imgSrc
            }).appendTo($(imgContainer)).hide().fadeIn(600);

        }
    }

    if (this.m_iSmileys > 0) {
        var imgSrc = smileImgs[this.m_iSmileys - 1].src ? smileImgs[this.m_iSmileys - 1].src : smileImgs[this.m_iSmileys - 1];
        $('<img />', {
            src: imgSrc
        }).appendTo($(imgContainer)).hide().fadeIn(600);

    }

    if (this.m_iCheckedStatus > 0) {

        var imgSrc = GenImgs[this.m_iCheckedStatus - 1].src ? GenImgs[this.m_iCheckedStatus - 1].src : GenImgs[this.m_iCheckedStatus - 1];
        $('<img />', {
            src: imgSrc
        }).appendTo($(imgContainer)).hide().fadeIn(600);

    }


    if (this.m_iPriority > 0) {
        var imgSrc = priorImgs[this.m_iPriority - 1].src ? priorImgs[this.m_iPriority - 1].src : priorImgs[this.m_iPriority - 1];
        $('<img />', {
            src: imgSrc
        }).appendTo($(imgContainer)).hide().fadeIn(600);
    }

    if (this.m_iCompletion > 0) {
        var imgSrc = complImgs[this.m_iCompletion - 1].src ? complImgs[this.m_iCompletion - 1].src : complImgs[this.m_iCompletion - 1];
        $('<img />', {
            src: imgSrc
        }).appendTo($(imgContainer)).hide().fadeIn(600);

    }
    if (this.m_strHyperlinkUrl && this.m_strHyperlinkUrl.length > 1) {

        if (this.m_strHyperlinkUrl && this.m_strHyperlinkUrl.length > 0) {
            var $this = this;
            var urlImage = $('<img />', {
                src: pImages[7],
                title: $this.m_strHyperlinkUrl

            }).appendTo($(imgContainer)).hide().fadeIn(600).on('click', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                //   SMM.hideMainMenu('fast');
                var url = $(this).attr('title');
                relocateToURL(url);
            });

        }
    } else if (this.m_urlType == 1) {
        this.m_strHyperlinkUrl = '';
    }


    //Notes Image and Pop Over
    if (this.m_strNode_Notes && this.m_strNode_Notes.length > 0) {


        var $this = this;
        var notesImg = $('<img />', {
            src: notesImage.src,
            className: 'notesImg',
            name: $this.m_strNode_Notes
        }).appendTo($(imgContainer)).hide().fadeIn(600).on('click', function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            //  SMM.hideMainMenu('fast');
            var ele = $(this);
            var data = $(this).attr('name');
            //  SMM.EBag().setCurrentPage($this);
            SMM.displayNotes(ele, data);
        });
        $this.m_notesimage = notesImg;


    } else if (this.m_strNode_Notes && this.m_strNode_Notes.length == 0) {

        this.m_strNode_Notes = '';
        this.m_notesWidth = 0;
        $this.m_notesimage = '';

    }


    //Multimedia Link URL


    var multimediaType = '';
    if (this.m_strMultimediaUrl && this.m_strMultimediaUrl.length > 0) {
        //return;
        var imageObj1;
        var multimediaUrl = this.m_strMultimediaUrl;
        var $this = this;
        if (this.m_strMultimediaType.length > 0) {
            multimediaType = this.m_strMultimediaType;
            switch (multimediaType) {
                case 'Audio':
                    imageObj1 = pImages[3].src;
                    break;
                case 'Video':
                    imageObj1 = pImages[4].src;
                    break;
                case 'Image':
                    imageObj1 = pImages[14].src;
                    break;
                case 'swf':
                    imageObj1 = pImages[5].src;
                    break;
                default:
                    imageObj1 = pImages[7].src;
                    break;

            }
        }
        //   

        var isEmbed = multimediaUrl.substr(multimediaUrl.length - 5);
        var isImageasNode = multimediaUrl.substr(multimediaUrl.length - 7);
        if (isEmbed == 'Embed' && multimediaType == 'Image') {
            SMM.createAttachImage(multimediaUrl, $this);
        } else if (isImageasNode == 'ImgNode' && multimediaType == 'Image') {
            SMM.createImageAsNode($this, multimediaUrl, 0);

        } else {

            var $this = this;
            var notesImg = $('<img />', {
                src: imageObj1,
                title: $this.m_strMultimediaUrl
            }).appendTo($(imgContainer)).hide().fadeIn(600).on('click tap', function (evt) {
                evt.preventDefault();
                evt.stopPropagation();
                loadMultiMediaContent($this, this);
            });
        }
    }

}

SMM.createAttachImage = function (src, page) {

    if (page == undefined) return;
    if (page.m_strMultimediaType != 'Image') return;
    var curPage = page;
    console.log('Loader Started..');
    curPage.m_iImageWidth = curPage.m_iImageWidth ? curPage.m_iImageWidth : $('#wInput').val();
    curPage.m_iImageHeight = curPage.m_iImageHeight ? curPage.m_iImageHeight : $('#hInout').val();
    curPage.isEmbed = true;
    var embedContainer = $(curPage.m_group).find('.embedContainer');
    $(embedContainer).empty().css('display', 'flex');
    $(embedContainer).empty().css('display', '-webkit-flex');
    $('<img  src="' + src + '" alt="Embed Image" />')
        .load(function () {
            if (curPage.m_iImageWidth && curPage.m_iImageWidth > 0) {
                if (curPage.m_iImageWidth > 800)
                    curPage.m_iImageWidth = 800;
                $(this).css('width', curPage.m_iImageWidth + 'px');
            } else {
                curPage.m_iImageWidth = ($(this).prop('naturalWidth'));
                if (curPage.m_iImageWidth > 800)
                    curPage.m_iImageWidth = 800;
                $(this).css('width', curPage.m_iImageWidth + 'px');
            }
            if (curPage.m_iImageHeight && curPage.m_iImageHeight > 0) {
                if (curPage.m_iImageHeight > 650)
                    curPage.m_iImageHeight = 650;
                $(this).css('height', curPage.m_iImageHeight + 'px');
            } else {
                curPage.m_iImageHeight = ($(this).prop('naturalHeight'));
                if (curPage.m_iImageHeight > 650)
                    curPage.m_iImageHeight = 650;
                $(this).css('height', curPage.m_iImageHeight + 'px');
            }

            $(curPage.m_group).find('.nodeContainer').css('height', '');
            curPage.m_selfWidth = $(curPage.m_group).outerWidth();
            curPage.m_selfHeight = $(curPage.m_group).outerHeight();
            $(curPage.m_group).data('page', curPage);

            curPage.parentEbag.layout();
        }).on('click', function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            loadMultiMediaContent(curPage, this);

        })
        .appendTo($(embedContainer)); //.hide().fadeIn();
}

SMM.createImageAsNode = function (page, src, isEditing) {
    var curPage = page;
    curPage.isEmbed = false;

    curPage.m_iImageWidth = curPage.m_iImageWidth ? curPage.m_iImageWidth : $('#wInput').val();
    curPage.m_iImageHeight = curPage.m_iImageHeight ? curPage.m_iImageHeight : $('#hInout').val();
    //  $(curPage.m_group).hide();
    $('<img  src="' + src + '" alt="Embed Image" />')
        .load(function () {
            if (curPage.m_iImageWidth && curPage.m_iImageWidth > 0) {
                if (curPage.m_iImageWidth > 800)
                    curPage.m_iImageWidth = 800;
                $(curPage.m_group).css('minWidth', curPage.m_iImageWidth + 'px');
            } else {
                curPage.m_iImageWidth = ($(this).prop('naturalWidth'));
                if (curPage.m_iImageWidth > 800)
                    curPage.m_iImageWidth = 800;
                $(curPage.m_group).css('minWidth', curPage.m_iImageWidth + 'px');
            }
            if (curPage.m_iImageHeight && curPage.m_iImageHeight > 0) {
                if (curPage.m_iImageHeight > 650)
                    curPage.m_iImageHeight = 650;
                $(curPage.m_group).css('minHeight', curPage.m_iImageHeight + 'px');
            } else {
                curPage.m_iImageHeight = ($(this).prop('naturalHeight'));
                if (curPage.m_iImageHeight > 650)
                    curPage.m_iImageHeight = 650;
                $(curPage.m_group).css('minHeight', curPage.m_iImageHeight + 'px');
            }
            // Instead of background images we are appending an image with absolute position with 100% 100% size
            //also changing nodeContainer height so that all conents will be placed in middle.
            // $(curPage.m_group).find("background-image", "url(" + this.src + ")");
            $(curPage.m_group).find('.imageAsNode').attr('src', this.src).hide().fadeIn(600);
            curPage.m_selfWidth = $(curPage.m_group).outerWidth();
            curPage.m_selfHeight = $(curPage.m_group).outerHeight();
            $(curPage.m_group).data('page', curPage);
            $(curPage.m_group).find('.nodeContainer').css('height', curPage.m_selfHeight + 'px');
            curPage.parentEbag.layout();

        });

    //$(curPage.m_group).css("background-image", "url(" + src + ")");
    //
    //curPage.parentEbag.layout();
}


//Add task Text here..
SMM.Page.prototype.addTaskContent = function () {
    var taskContainer = $(this.m_group).find('.taskContainer');
    if (this.m_strStartDate.length > 0 || this.m_strDueDate.length > 0 || this.m_strResourceType.length > 0 || this.m_strDurationType.length > 0 || this.m_iResourceDuration > 0) {
        var taskTest = getTaskText(this);
        if (taskTest && taskTest.length > 0) {
            $(taskContainer).show();
            $(taskContainer).append('<label>' + taskTest + '</label>').hide().fadeIn(600);
        } else {
            $(taskContainer).hide().empty();
        }
    }
}

SMM.Page.prototype.draw = function () {

    var parentPage;
    var thisPage = this;
    if (this.m_parentPage && this.m_parentPage.m_group) {
        parentPage = this.m_parentPage.m_group;
    }

    var hasNode = this.m_group;
    if (hasNode && hasNode != "") {
        try {
            $(hasNode).remove();
        } catch (err) {
            console.log('Unable to remove nodes');
        }

    }
    this.m_group = undefined;

    var stage = document.getElementById('container');
    var node = document.createElement('div');
    var imgContainer = document.createElement('div');
    imgContainer.className = 'imgContainer';
    var taskContainer = document.createElement('div');
    taskContainer.className = 'taskContainer';
    node.classList.add("node");
    node.style.backgroundColor = thisPage.m_strNodeShapeFill;
    if (this.m_iGradientType > 0) {
        var cssText = getGradientClr(thisPage.m_strNodeShapeFill, thisPage.m_iGradientType);
        node.style.background = cssText;
        node.style.background = '-webkit-' + cssText;
        node.style.background = '-moz-' + cssText;
        node.style.background = '-o-' + cssText;

    }


    var textContent = document.createElement('label');
    textContent.className = 'text';
    //  textContent.innerHTML = (pageVal).replace(/\n/g, "<br/>");


    var textAlign = thisPage.m_textFormat.m_strNode_TextAlign;
    if ((textAlign).indexOf('$$') != -1) {
        textAlign = textAlign.split('$$')[0];
        this.m_textDirection = 'rtl';
    }
    //Node Text formatting
    textContent.style.fontFamily = thisPage.m_textFormat.m_strNode_TextFontFamily;
    textContent.style.textAlign = textAlign; //thisPage.m_textFormat.m_strNode_TextAlign;
    textContent.style.color = thisPage.m_textFormat.m_strNode_TextColor;
    textContent.style.fontSize = thisPage.m_textFormat.m_iNode_TextSize + 'px';
    textContent.style.fontStyle = thisPage.m_textFormat.m_iIsItalic == 0 ? 'normal' : 'italic';
    textContent.style.fontWeight = thisPage.m_textFormat.m_iIsBold == 0 ? 'normal' : 'bold';
    textContent.style.textDecoration = thisPage.m_textFormat.m_iIsUnderline == 0 ? 'normal' : 'underline';






    //MainContainer for all Contents in Node
    var container = document.createElement('div');
    container.className = 'nodeContainer';


    //Based on allign needs to cahnge alignmnet of textContent
    // textContent.style.padding = '0px 6px';

    //Background Images and Embed Images
    var embedImageContainer = document.createElement('div');
    embedImageContainer.className = 'embedContainer';
    node.appendChild(embedImageContainer);

    var imageAsNode = document.createElement('img');
    imageAsNode.className = 'imageAsNode';
    node.appendChild(imageAsNode);


    //Internet Explorer FallBack code here
    //All non supported shapes in IE will become default shape

    if (isInternetExplorer) {
        if (this.m_iNodeShape >= 4)
            this.m_iNodeShape = 2;
    }


    //notes of the node
    var bdrClr = shadeColor(thisPage.m_strNodeShapeFill, -5);
    node.style.borderColor = bdrClr;
    if (this.m_iNodeShape == 1)
        node.style.borderRadius = '0px';
    else if (this.m_iNodeShape == 2)
        node.style.borderRadius = '8px';
    //Change Shapes here
    else if (this.m_iNodeShape == 3)

    {
        this.m_selfWidth = this.m_selfWidth * 1.11;
        this.m_selfHeight = this.m_selfHeight * 1.25;
        node.classList.add("oval");
    } else if (this.m_iNodeShape == 4) {
        node.classList.add('polygon');
    } else if (this.m_iNodeShape == 5) {


        node.classList.add('bubbleShape');
        //  node.style.borderColor = thisPage.m_strNodeShapeFill + 'transparent';
    } else if (this.m_iNodeShape == 6) {
        node.classList.add('arcRect');
    } else if (this.m_iNodeShape == 7) {
        node.classList.add('octagon');
    } else if (this.m_iNodeShape == 8) {
        node.classList.add('triangle');
    } else if (this.m_iNodeShape == 9) {
        node.classList.add('rhoumbus');
    } else
        node.style.borderRadius = '8px';



    node.appendChild(container);
    container.appendChild(imgContainer);
    container.appendChild(textContent);
    container.appendChild(taskContainer);


    //Text of the node
    var pageVal = this.m_textFormat.m_strNode_text;
    //pageVal = pageVal.replace(/&lt;/g, "<");
    //pageVal = pageVal.replace(/&gt;/g, ">");
    //pageVal = pageVal.replace(/\n/g, "<br/>");
    pageVal = pageVal.nl2br();

    $(textContent).html(pageVal);
    $(textContent).attr("dir", "auto");

    //Alignment
    if (textAlign == 'center')
        container.style.justifyContent = 'center';
    else if (textAlign == 'left')
        container.style.justifyContent = 'flex-start';
    if (textAlign == 'right')
        container.style.justifyContent = 'flex-end';

    if (this.m_isCollapsed) {
        var hasClass = $(node).hasClass('nodeCollapse');
        if (hasClass == false) {
            $(node).addClass('nodeCollapse');
        }
    }


    //Alignment
    //Single Line font Align Code

    var match = (this.m_textFormat.m_strNode_text).split('/n').length;
    if (match) {
        if (textAlign == 'center')
            container.style.margin = '0px';
        else if (textAlign == 'left') {
            textContent.style.marginLeft = '-10px';
            textContent.style.marginRight = '20px';
        }

        if (textAlign == 'right') {
            textContent.style.marginLeft = '20px';
            textContent.style.marginRight = '-10px';
        }

    }


    // $(textContent).attr('dir','auto');

    if (this.m_textDirection == 'rtl') {
        $(textContent).css('direction', 'rtl');
    }


    // setDirection($(textContent), true);
    this.m_group = node;
    $(node).appendTo(stage);
    this.loadImages();
    this.addTaskContent();
    this.m_selfWidth = $(node).outerWidth();
    this.m_selfHeight = $(node).outerHeight();

    $(node).find('.nodeContainer').css('height', '');
    if (this.m_iNodeShape > 2) {
        $(node).find('.nodeContainer').css('height', this.m_selfHeight + 'px');
    } else {
        $(node).find('.nodeContainer').css('height', '');
    }

    //Shapes Adjustment
    if (this.m_iNodeShape == 9) {
        this.m_selfHeight = this.m_selfWidth;
        $(node).css('height', this.m_selfHeight + 'px');
        $(node).find('.nodeContainer').css('height', this.m_selfHeight + 'px');
    }

    if (this.m_iNodeShape == 8) {
        this.m_selfWidth = 1.5 * this.m_selfWidth;
        this.m_selfHeight = 1.1 * this.m_selfHeight;
        $(node).css('width', this.m_selfWidth + 'px');
        $(node).css('height', this.m_selfHeight + 'px');
        $(node).find('.nodeContainer').css('height', this.m_selfHeight / 2 + 'px');
    }

    if (this.m_iNodeShape == 6 || this.m_iNodeShape == 7) {
        this.m_selfWidth = this.m_selfWidth * 1.15;
        $(node).css('width', this.m_selfWidth + 'px');
        //$(node).find('.nodeContainer').css('height', this.m_selfHeight+'px');
    }




    this.m_selfWidth = $(node).outerWidth();
    this.m_selfHeight = $(node).outerHeight();

    $(node).data('page', this);

}